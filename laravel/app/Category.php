<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $primaryKey = 'cat_id';

	protected $fillable = ['cat_title', 'cat_delete','cat_image'];

	public $timestamps = false;

	public function subCategories(){
		return $this->hasMany(\App\SubCategory::class, 'sc_cat_id');
	}
	public function packages(){
		return $this->hasMany(\App\Package::class, 'pack_cat_id');
	}
}
