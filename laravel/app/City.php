<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $primaryKey = 'city_id';

    protected $fillable = ['city_name', 'city_state_id'];

    public $timestamps = false;
}
