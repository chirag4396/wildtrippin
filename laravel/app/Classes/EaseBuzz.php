<?php
namespace App\Classes;

use App\Classes\Payment;
use App\Classes\Misc;
use App\Classes\Curl;

class EaseBuzz
{   
    public function easepay_page ( $params, $salt, $env='test' ){
        $result = $this->pay( $params, $salt, $env);
        return Misc::show_page( $result );
    }

    public function pay ( $params, $salt, $env='test'){
        if ( ! is_array( $params ) ) throw new Exception( 'Pay params is empty' );

        if ( empty( $salt ) ) throw new Exception( 'Salt is empty' );

        $payment = new Payment( $salt, $env );
        $result = $payment->pay( $params );
        unset( $payment );
        return $result;
    }

    public function response($params, $salt) {
        if (!is_array($params))
            throw new \Exception('response params is empty');

        if (empty($salt))
            throw new \Exception('Salt is empty');

        if (empty($params['status']))
            throw new \Exception('Status is empty');

        $response = new Response($salt);
        $result = $response->get_response($_POST);
        unset($response);

        return $result;
    }
    
    function __construct()
    {

        if (!function_exists('curl_init')) :

            define('CURLOPT_URL', 1);
            define('CURLOPT_USERAGENT', 2);
            define('CURLOPT_POST', 3);
            define('CURLOPT_POSTFIELDS', 4);
            define('CURLOPT_RETURNTRANSFER', 5);
            define('CURLOPT_REFERER', 6);
            define('CURLOPT_HEADER', 7);
            define('CURLOPT_TIMEOUT', 8);
            define('CURLOPT_CONNECTTIMEOUT', 9);
            define('CURLOPT_FOLLOWLOCATION', 10);
            define('CURLOPT_AUTOREFERER', 11);
            define('CURLOPT_PROXY', 12);
            define('CURLOPT_PORT', 13);
            define('CURLOPT_HTTPHEADER', 14);
            define('CURLOPT_SSL_VERIFYHOST', 15);
            define('CURLOPT_SSL_VERIFYPEER', 16);

            function curl_init($url = false) {
                return new Curl($url);
            }

            function curl_setopt(&$ch, $name, $value) {
                $ch->setopt($name, $value);
            }

            function curl_exec($ch) {
                return $ch->exec();
            }

            function curl_close(&$ch) {
                unset($ch);
            }

            function curl_errno($ch) {
                return $ch->error;
            }

            function curl_error($ch_error) {
                return "Could not open socket";
            }

            function curl_getinfo($ch, $opt = NULL) {
                return $ch->info;
            }

            function curl_setopt_array(&$ch, $opt) {
                $ch->setoptArray($opt);
            }

        endif;

    }
}

