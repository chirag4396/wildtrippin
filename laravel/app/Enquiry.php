<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $primaryKey = 'eq_id';

    protected $fillable = ['eq_user','eq_pack_id','eq_email','eq_mobile','eq_query'];

    CONST CREATED_AT = 'eq_created_at';
    CONST UPDATED_AT = 'eq_updated_at';

    public function package(){
		return $this->belongsTo(\App\Package::class, 'eq_pack_id');
	}
}
