<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDateprice extends Model
{
    protected $primaryKey = 'edp_id';

    protected $fillable = ['edp_date','edp_date_price','edp_pack_id'];

    public $timestamps = false;
}
