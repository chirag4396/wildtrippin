<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{

	protected $primaryKey = 'facility_id';

	protected $fillable = ['facility_name', 'facility_img_path', 'facility_status'];

	public $timestamps = false;
}
