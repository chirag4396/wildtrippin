<?php

namespace App\Http\Controllers;

use App\Category;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class CategoryController extends Controller
{
	use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/categories/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategoryListing(Request $r)
    {
        $categoryList=Category::take(10)->where('cat_title','LIKE','%'.request('q').'%')->get();
        foreach ($categoryList as $key => $value) {
            $json[] = ['id'=>$value['cat_id'], 'text'=>$value['cat_title']];
        }

            echo json_encode($json);
    }
    public function allCategories (Request $r){
        $categories=Category::where('cat_delete',0)->orderBy('cat_id','DESC');

        $datatable = DataTables::of($categories)->addColumn('name', function($cat){
            return str_limit($cat->cat_title,30);
        })->addColumn('image', function($cat){
            return '<img src="'.asset( isset($cat->cat_image) ? $cat->cat_image : 'images/no-image.png').'" class="pro-img">';
        })->addColumn('action', function($cat){
            return '<a class="btn btn-info btn-xs" href = "'.route('admin.category.edit', ['id' => $cat->cat_id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.category.destroy', ['id' => $cat->cat_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name', 'image', 'action']);

        foreach (['cat_title', 'cat_image', 'cat_delete'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }
    public function index(Request $request)
    {

        return view('admin.view_categories');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form_layout')->with(['id' => 'category', 'process' => 'add']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {                
        try {
            $cat = $this->changeKeys('cat_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $cat['cat_title'], 'image', [$this->path]);
                $cat['cat_image'] = $regular;
            }
            if(Category::create($cat)){
                $this->res['msg'] = 'success';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //return Category::find($category->cat_id);exit;
        return view('admin.form_layout')->with(['id' => 'category', 'process' => 'edit','record'=>Category::find($category->cat_id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Category $category)
    {

        try {
            $cat = $this->changeKeys('cat_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $cat['cat_title'], 'image', [$this->path], [$category->cat_image]);
                $cat['cat_image'] = $regular;
            }
            if($category->update($cat)){
                $this->res['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // $category->subCategories()->packages()->update(['pack_delete' => 1]);
        $category->subCategories()->update(['sc_delete' => 1]);
        $category->packages()->update(['pack_delete' => 1]);
        $category->update(['cat_delete' => 1]);

        return redirect()->back();
    }
}
