<?php

namespace App\Http\Controllers;

use App\City;
use App\State;
use App\Country;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }

    function ajaxState($id)
    { 
       
        $stateData=State::where('state_country_id',$id)->get();
       $str='<option value="-1" selected="">--select state--</option>';
       foreach ($stateData as $key => $value) {
           $str.='<option value='.$value['state_id'].'>'.$value['state_name'].'</option>';
       }
       echo $str;exit;
    }
     function ajaxcity($id)
    { 
       
        $stateData=City::where('city_state_id',$id)->get();
       $str='<option value="-1"  selected="">--select city--</option>';
       foreach ($stateData as $key => $value) {
           $str.='<option value='.$value['city_id'].'>'.$value['city_name'].'</option>';
       }
       echo $str;exit;
    }
}
