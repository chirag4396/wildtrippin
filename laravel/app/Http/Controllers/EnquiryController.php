<?php

namespace App\Http\Controllers;

use App\Enquiry;
use App\Package;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_enquiries');
    }
    public function allEnquiries()
    {
        if(Auth::user()->type==101)
        {
          $enquiries=Enquiry::orderBy('eq_id','DESC');  
        }
        else if(Auth::user()->type==103)
        {
            $enquiries=Enquiry::join('packages', function($join) {
              $join->on('enquiries.eq_pack_id', '=', 'packages.pack_id');
            })->where('packages.pack_user',Auth::user()->id)->get();
            //$enquiries=Enquiry::orderBy('eq_id','DESC');  
        }
        
        $datatable = DataTables::of($enquiries)->addColumn('packagename', function($enq){
            return str_limit((!is_null($packtitle=Package::find($enq->eq_pack_id)->pack_title)?$packtitle:''),30);
        })->addColumn('name', function($enq){
            return str_limit($enq->eq_user,30);
        })->addColumn('mobile', function($enq){
            return $enq->eq_mobile;
        })->addColumn('action', function($enq){
            return '<button class="btn btn-warning btn-xs" onclick = "enquiryView('.$enq->eq_id.');"><i class="fa fa-eye"></i> View</button> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.enquiry.destroy', ['id' => $enq->eq_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name', 'mobile','action']);

        foreach (['eq_email', 'eq_query','eq_user','eq_created_at','eq_updated_at'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eq_id=Enquiry::create($request->all())->eq_id;
        if(!is_null($eq_id))
        {
            echo "success";exit;
        }
        echo "failure";exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Enquiry $enquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(Enquiry $enquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enquiry $enquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enquiry $enquiry)
    {
        $enquiry->delete();
        return redirect()->back();
    }

    public function ajaxViewEnquiry(Request $request)
    {
        $enquiry_details=Enquiry::find($request->input('id'));
        $enquiryStr='';
        $datetime=date('jS M Y h:i:s a',strtotime($enquiry_details->eq_created_at));
        $enquiryStr.='<table  class="table"><thead><tr><th width="60%">Posted Date Time:</th><td width="40%">'.$datetime.'</td></tr><tr><th width="40%">Name</th><th width="40%">Email</th><th width="40%">Mobile</th></tr></thead>';
        $enquiryStr.='<tbody><tr><td>'.$enquiry_details->eq_user.'</td><td>'.$enquiry_details->eq_email.'</td><td>'.$enquiry_details->eq_mobile.'</td>
        </tr><tr><td colspan="3"></td></tr><tr><td width="40%" style="font-weight: bold;">Content:</td><td width="100%" colspan="2">'.$enquiry_details->eq_query.'</td></tr></tbody></table>';
       return response()->json($enquiryStr);
    }
}
