<?php

namespace App\Http\Controllers;

use App\EventDateprice;
use Illuminate\Http\Request;

class EventDatepriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventDateprice  $eventDateprice
     * @return \Illuminate\Http\Response
     */
    public function show(EventDateprice $eventDateprice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventDateprice  $eventDateprice
     * @return \Illuminate\Http\Response
     */
    public function edit(EventDateprice $eventDateprice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventDateprice  $eventDateprice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventDateprice $eventDateprice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventDateprice  $eventDateprice
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventDateprice $eventDateprice)
    {
        //
    }
}
