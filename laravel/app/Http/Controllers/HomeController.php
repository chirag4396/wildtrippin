<?php

namespace App\Http\Controllers;

use App\Package;
use App\SubCategory;
use App\Category;
use Session;
use App\SearchKeyword;
use App\EventDateprice;
use DateTime;
use App\PackageImage;
use App\City;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function findPackageOnSubcategory(Request $request,$id)
    {
        $sc_id=$id;
        $packageData=Package::whereRaw('FIND_IN_SET(?,Pack_sc_id)',[$id])->where('pack_delete',0)->get();
        $keyword=array();
        foreach ($packageData as $key => $value) {
            $packKeywords=array();
            $ids=explode(",", $value['pack_keywords']);
            foreach ($ids as $key => $value) {
                $title=SearchKeyword::find($value)->sk_title;
                if(!in_array($title, $keyword))
                {
                    array_push($keyword, $title);
                }
            }
        }
        $data['search_keywords']=$keyword;
        $data['packageData']=$packageData;
        Session::put('sc_id', $id);
        $cat_id=SubCategory::find($id)->sc_cat_id;
        Session::put('cat_id',$cat_id);
        return view('users.listing')->with($data);
    }

    public function getPackageViewDetail($id,Request $request)
    {
        $pack_id=$id;
        $packageDetails=Package::find($id);
        $packageDetails['package_images']=$packageDetails->packageImages()->where('pi_delete',0)->get();
        $packageDetails['itinerary_days']=$packageDetails->itinerarydays()->where('it_day_delete',0)->get();
        $packageDetails['itinerary_acts']=$packageDetails->itineraryctivities()->where('it_act_delete',0)->get();
        $packageDetails['pack_features']=$packageDetails->packageFeatures()->where('pf_delete',0)->get();
        $data['packageDetails']=$packageDetails;
        //
        $session_sc_id = session('sc_id');
        $data['related_packages']=$this->findRelatedPackages($session_sc_id);
        $data['reviewsList']=$packageDetails->reviews()->orderBy('rv_id','desc')->paginate(2);
        foreach ($data['reviewsList'] as $key => $value) {
            $data['reviewsList'][$key]['addedDate']=$this->time_elapsed_string(($value->rv_created_at));
        }
        return view('users.detail')->with($data);
    }

    public function findRelatedPackages($id)
    {
        $sc_id=$id;
        $packageData=Package::take(3)->whereRaw('FIND_IN_SET(?,Pack_sc_id)',[$id])->where('pack_delete',0)->get();
       
        return $packageData;
    }
    

     public function findPackageOnCategory(Request $request,$id)
    {
        $packageData=Category::find($id)->packages()->get();
        $keyword=array();
        foreach ($packageData as $key => $value) {
            $packKeywords=array();
            $ids=explode(",", $value['pack_keywords']);
            foreach ($ids as $key => $value) {
                $title=SearchKeyword::find($value)->sk_title;
                if(!in_array($title, $keyword))
                {
                    array_push($keyword, $title);
                }
            }
        }
        $data['search_keywords']=$keyword;
        $data['packageData']=$packageData;
        
        return view('users.listing')->with($data);
    }

     public function findPackageOnService(Request $request,$id)
    {
        $packageData=Package::whereRaw('FIND_IN_SET(?,pack_s_id)',[$id])->where('pack_delete',0)->get();
        $keyword=array();
        foreach ($packageData as $key => $value) {
            $packKeywords=array();
            $ids=explode(",", $value['pack_keywords']);
            foreach ($ids as $key => $value) {
                $title=SearchKeyword::find($value)->sk_title;
                if(!in_array($title, $keyword))
                {
                    array_push($keyword, $title);
                }
            }
        }
        $data['search_keywords']=$keyword;
        $data['packageData']=$packageData;
        
        return view('users.listing')->with($data);
    }

    public function findPriceOfEventDate(Request $request)
    {
       $eventdateData=EventDateprice::where('edp_id',$request['id'])->first();
        echo $eventdateData['edp_date_price'];exit;
    }

 function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);
 
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
 
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
 
    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

    function searchResult(Request $request)
    {
        
        $sql = Package::where('pack_delete',0)->where(function($sql) use ($request) {
            if($request->searchtext){
                $sql->where('pack_title', 'LIKE', $request->searchtext.'%');
            }
            if($request->searchadvancetext){
                $sql->where('pack_title', 'LIKE', $request->searchadvancetext.'%');
            }
            if($request->days){
                $sql->where('pack_days', '<=', [$request->days]);
            }
            if($request->month){
                $sql->whereRaw('extract(month from pack_startdate) = ?', [$request->month] );
            }
            if($request->cat_id){
                $sql->where('pack_cat_id', [$request->cat_id] );
            }
        })->get();
        $packagePrice=array();
         $keyword=array();
            foreach ($sql as $key => $value) {
                if($value->pack_type=='package')
                {
                    if(!in_array($value->pack_price, $packagePrice))
                    {
                        array_push($packagePrice, $value->pack_price);
                    }
                }
                if($value->pack_type=='event')
                {
                    $eventDataPrice=EventDateprice::where('edp_pack_id',$value->pack_id)->get();
                    foreach ($eventDataPrice as $key1 => $value1) {
                        if(!in_array($value1->edp_date_price, $packagePrice))
                        {
                            array_push($packagePrice, $value1->edp_date_price);
                        }
                    }
                }
                $packKeywords=array();
                $ids=explode(",", $value['pack_keywords']);
                foreach ($ids as $key => $value) {
                    $title=SearchKeyword::find($value)->sk_title;
                    if(!in_array($title, $keyword))
                    {
                        array_push($keyword, $title);
                    }
                }
            }
            if($request->days){
             $data['noOfDays']=$request->days;
            }
            if($request->month){
             $data['month']=$request->month;
            }
            if($request->cat_id){
             $data['name']=Category::find($request->cat_id)->cat_title;
            }
            if($request->days){
             $data['days']=$request->days;
            }
            if($request->searchtext){
             $data['narrowsearch']=$request->searchtext;
            }
            if($request->searchadvancetext){
             $data['narrowsearch']=$request->searchadvancetext;
            }
            if($request->cat_id){
             $data['cat_id']=$request->cat_id;
            }
            $data['max']=max($packagePrice);
            $data['min']=min($packagePrice);
            $data['max']=max($packagePrice);
            $data['search_keywords']=$keyword;
            $data['packageData']=$sql;
            
            return view('users.listing')->with($data);
    }

    function getPackageOnPrice(Request $request)
    {
        $sql = Package::where('pack_delete',0)->where(function($sql) use ($request) {
            if($request->searchtext){
                $sql->where('pack_title', 'LIKE', $request->searchtext.'%');
            }
            if($request->searchadvancetext){
                $sql->where('pack_title', 'LIKE', $request->searchadvancetext.'%');
            }
            if($request->days){
                $sql->where('pack_days', '<=', [$request->days]);
            }
            if($request->month){
                $sql->whereRaw('extract(month from pack_startdate) = ?', [$request->month] );
            }
            if($request->cat_id){
                $sql->where('pack_cat_id', [$request->cat_id] );
            }
        })->get();
        $min=$request->min;
       // echo $request->price;exit;
        $packagePrice=array();
        $packageArray=array();
         $keyword=array();
            foreach ($sql as $key => $value) {
                //echo $sql[$key]->pack_id.'------';
                // echo $request->price .'----------';
                    
                if($value->pack_type=='package')
                {
                   // echo $value->pack_price.'------';

                   if($value->pack_price<=$request->price && $value->pack_price>=$min)
                   {
                        $packageArray=array_merge($packageArray,(array)$value);
                   }
                   else
                   {
                        $sql->forget($key);
                   }
                }
                else if($value->pack_type=='event')
                {
                    $eventDataPrice=EventDateprice::where('edp_pack_id',$value->pack_id)->get();
                    if(count($eventDataPrice)>0)
                    {
                        foreach ($eventDataPrice as $key1 => $value1) {
                            if($value1->edp_date_price<=$request->pack_price && $value1->edp_date_price>=$min)
                            {
                               $packageArray=array_merge($packageArray,(array)$value);
                            }
                        }
                    }
                    else
                    {
                        $sql->forget($key);
                    }
                }
                else
                {
                    $sql->forget($key);
                }
                //echo $request->min .'----------';
                
            }
            $str='';
            
        foreach ($sql as $pk => $pv) {
           // echo $sql[$pk]->pack_id.'------';
            $image=PackageImage::where("pi_pack_id",$pv->pack_id)->first();
            $city=City::find($pv->pack_city)->city_name; 
            $url=(isset($image))?url($image->pi_img_path):url('images/no-image.png');
            $redirecturl=url("/get-package-detail/".$pv->pack_id);
            $str.=
             '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all"><div class="cuadro_intro_hover " style="background-color:#fff;"><p class="p1">
            <img src="'.$url.'" class="img-responsive" alt=""></p><div class="caption"><div class="blur"></div><div class="caption-text"><a class=" btn btn-default button-book" href="'.$redirecturl.'"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
              <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;"><span class="span-disc">Jumbo Discount Price From</span>&#8377;'.$pv->pack_price.'</h3><p>'.$pv->pack_title.'</p><p>'.$pv->pack_days.'  Days/'.$pv->pack_nights .'nights:<span class="span-disc">ASHG</span></p></div></div></div><p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>'.$city.'</p></div>';
        }
      //  exit;
        echo $str;exit;
            
    }
}
