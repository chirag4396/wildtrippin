<?php

namespace App\Http\Controllers;

use App\IndexCategory;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class IndexCategoryController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/index-categories/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function allIndexCategories(Request $r){
        $indexCat=IndexCategory::where('ic_delete',0)->orderBy('ic_id','DESC');

        $datatable = DataTables::of($indexCat)->addColumn('name', function($icat){
            return str_limit($icat->ic_title,30);
        })->addColumn('link', function($icat){
            return str_limit($icat->ic_link,30);
        })->addColumn('image', function($icat){
            return '<img src="'.asset( isset($icat->ic_image) ? $icat->ic_image : 'images/no-image.png').'" class="pro-img">';
        })->addColumn('action', function($icat){
            return '<a class="btn btn-info btn-xs" href = "'.route('admin.index-category.edit', ['id' => $icat->ic_id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-info btn-xs" onclick = "deleteModal(\''.route('admin.index-category.destroy', ['id' => $icat->ic_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name','link', 'image', 'action']);

        foreach (['ic_title', 'ic_image', 'ic_delete','ic_link'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }
    public function index()
    {
        return view('admin.view_index_categories');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form_layout')->with(['id' => 'index-category', 'process' => 'add']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
         try {
            $cat = $this->changeKeys('ic_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $cat['ic_title'], 'image', [$this->path]);
                $cat['ic_image'] = $regular;
            }
            if(IndexCategory::create($cat)){
                $this->res['msg'] = 'success';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IndexCategory  $indexCategory
     * @return \Illuminate\Http\Response
     */
    public function show(IndexCategory $indexCategory)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IndexCategory  $indexCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(IndexCategory $indexCategory)
    {
        
         return view('admin.form_layout')->with(['id' => 'index-category', 'process' => 'edit','record'=>IndexCategory::find($indexCategory->ic_id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IndexCategory  $indexCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, IndexCategory $indexCategory)
    {

         try {
            $icat = $this->changeKeys('ic_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $icat['ic_title'], 'image', [$this->path], [$indexCategory->ic_image]);
                $icat['ic_image'] = $regular;
            }
            if($indexCategory->update($icat)){
                $this->res['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IndexCategory  $indexCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndexCategory $indexCategory)
    {
        
         $indexCategory->update(['ic_delete' => 1]);

        return redirect()->back();
    }
}
