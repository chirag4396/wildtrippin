<?php

namespace App\Http\Controllers;

use App\ItineraryActivities;
use Illuminate\Http\Request;

class ItineraryActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItineraryActivities  $itineraryActivities
     * @return \Illuminate\Http\Response
     */
    public function show(ItineraryActivities $itineraryActivities)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItineraryActivities  $itineraryActivities
     * @return \Illuminate\Http\Response
     */
    public function edit(ItineraryActivities $itineraryActivities)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItineraryActivities  $itineraryActivities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItineraryActivities $itineraryActivities)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItineraryActivities  $itineraryActivities
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItineraryActivities $itineraryActivities)
    {
        //
    }
}
