<?php

namespace App\Http\Controllers;

use App\ItineraryActivity;
use Illuminate\Http\Request;

class ItineraryActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItineraryActivity  $itineraryActivity
     * @return \Illuminate\Http\Response
     */
    public function show(ItineraryActivity $itineraryActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItineraryActivity  $itineraryActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(ItineraryActivity $itineraryActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItineraryActivity  $itineraryActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItineraryActivity $itineraryActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItineraryActivity  $itineraryActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItineraryActivity $itineraryActivity)
    {
        //
    }
}
