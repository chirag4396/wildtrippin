<?php

namespace App\Http\Controllers;

use Auth;
use App\Package;
use App\PackageImage;
use App\PackageFeature;
use App\Itinerary;
use App\ItineraryDay;
use App\ItineraryActivity;
use App\City;
use App\State;
use App\Country;
use App\SearchKeyword;
use App\EventDateprice;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class PackageController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];

    protected $path = 'images/packages/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function allPackages (Request $r){
        if(Auth::user()->type==101)
        {
          $packages=Package::where('pack_delete',0)->where('pack_type','package')->orderBy('pack_id','DESC');  
        }
        else if(Auth::user()->type==103)
        {
            $packages=Package::where('pack_delete',0)->where('pack_type','package')->where('pack_user',Auth::user()->id)->orderBy('pack_id','DESC');  
        }
        
        $datatable = DataTables::of($packages)->addColumn('vendor', function($pack){
            return str_limit(User::find($pack->pack_user)->name,30);
        })->addColumn('name', function($pack){
            return str_limit($pack->pack_title,30);
        })->addColumn('price', function($pack){
            return $pack->pack_price;
        })->addColumn('discount', function($pack){
            return $pack->pack_discount;
        })->addColumn('action', function($pack){
            return '<a class="btn btn-info btn-xs" href = "'.route('admin.package.edit', ['id' => $pack->pack_id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.package.destroy', ['id' => $pack->pack_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['vendor','name', 'price','discount', 'action']);

        foreach (['pack_title', 'pack_sub_title', 'pack_description','pack_price','pack_discount','pack_facilities','pack_inclusions','pack_exclusions','pack_delete','pack_created_at','pack_updated_at'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }
    public function allEvents (Request $r){
         if(Auth::user()->type==101)
        {
            $packages=Package::where('pack_delete',0)->where('pack_type','event')->orderBy('pack_id','DESC');
        }
         if(Auth::user()->type==103)
        {
            $packages=Package::where('pack_delete',0)->where('pack_type','event')->where('pack_user',Auth::user()->id)->orderBy('pack_id','DESC');
        }

        $datatable = DataTables::of($packages)->addColumn('name', function($pack){
            return str_limit($pack->pack_title,30);
        })->addColumn('price', function($pack){
            return $pack->pack_price;
        })->addColumn('discount', function($pack){
            return $pack->pack_discount;
        })->addColumn('action', function($pack){
            return '<a class="btn btn-info btn-xs" href = "'.route('admin.package.edit', ['id' => $pack->pack_id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.package.destroy', ['id' => $pack->pack_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name', 'price','discount', 'action']);

        foreach (['pack_title', 'pack_sub_title', 'pack_description','pack_price','pack_discount','pack_facilities','pack_inclusions','pack_exclusions','pack_delete','pack_created_at','pack_updated_at'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }
     public function index()
    {
        return view('admin.view_packages');
    }
     public function indexevents()
    {
        return view('admin.view_events');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['images']=array();
        
        return view('admin.form_layout')->with(['id' => 'package', 'process' => 'add','record'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {

        try {
            
            $package = $this->changeKeys('pack_', $r->all());
            
            $package['pack_facilities']=implode("|", $package['pack_facilities']);
            $package['pack_sc_id']=implode(",", $package['pack_sc_id']);
            $package['pack_s_id']=implode(",", $package['pack_s_id']);
            $package['pack_keywords']=implode(",", $package['pack_keywords']);
            $package['pack_user']=Auth::user()->id;
            //return $package;
            $pack_id=Package::create($package)->pack_id;

            if($pack_id>0){
                $pack=Package::find($pack_id);
                $pack->subCategories()->attach($r->sc_id);
                if($package['pack_type']=='event')
                {
                    for($dp=0;$dp<count($package['pack_date']);$dp++)
                    {
                        $eventDatePrice=array();
                        
                        $eventDatePrice['edp_date']=date('Y-m-d',strtotime($package['pack_date'][$dp]));
                        $eventDatePrice['edp_date_price']=$package['pack_date_price'][$dp];
                        $eventDatePrice['edp_pack_id']=$pack_id;
                       
                        $edp_id=EventDateprice::create($eventDatePrice)->edp_id;
                    }
                }

                for($i=1; $i <=2 ; $i++) { 
                   //return $package['pack_it_sub_title'.$i];
                    for($k=0; $k < count($package['pack_it_sub_title'.$i]) ; $k++)
                    {
                        if(isset($package['pack_it_sub_title'.$i][$k]) && isset($package['pack_it_desc'.$i][$k]))
                        {
                             $middle=($i==1)?'day':'act';

                             $itinery=array();
                            $itinery[$k]['it_'.$middle.'_sub_title']=$package['pack_it_sub_title'.$i][$k];
                            $itinery[$k]['it_'.$middle.'_desc']=$package['pack_it_desc'.$i][$k];
                            $itinery[$k]['it_'.$middle.'_pack_id']=$pack_id;
                            $itinery[$k]['it_'.$middle.'_delete']=0;
                            $itinery[$k]=(array)$itinery[$k];
                            if($i==1)
                            {
                                ItineraryDay::create($itinery[$k]);
                            }
                            else
                            {
                                ItineraryActivity::create($itinery[$k]);
                            }
                        }
                       
                    }
                }
                if ($package['pack_features']!='') {
                    $packageFeature['pf_title'] = $package['pack_features'];
                    $packageFeature['pf_pack_id'] = $pack_id;
                    PackageFeature::create($packageFeature);
                    
                }
                
                for ($j=0; $j <$package['pack_total_images'] ; $j++) { 
                        $packageImage=array();
                        if ($r->hasFile('image'.$j)) {
                            list($regular) = $this->uploadFiles($r, $package['pack_title'], 'image'.$j,[$this->path]);
                            $packageImage['pi_img_path'] = $regular;
                            $packageImage['pi_pack_id'] = $pack_id;
                            PackageImage::create($packageImage);
                    }
                }
                $this->res['msg'] = 'success';
                $this->res['pack_type']=$r->type;
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        ini_set('memory_limit','512M');
       
        $package['features']=PackageFeature::where('pf_pack_id',$package['pack_id'])->where('pf_delete',0)->first();
       

        $package['pack_state']=City::find($package['pack_city'])->city_state_id;
        $package['pack_country']=State::find($package['pack_state'])->state_country_id;
       /* echo "<pre>";
        print_r($package);exit;*/
        return view('admin.form_layout')->with(['id' => 'package', 'process' => 'edit','record'=>$package]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Package $package)
    {
        try {
          
            $pack_id=$package->pack_id;
            $packages = $this->changeKeys('pack_', $r->all());

            $packages['pack_facilities']=implode("|", $packages['pack_facilities']);
            $packages['pack_sc_id']=implode(",", $packages['pack_sc_id']);
            $package['pack_s_id']=implode(",", $package['pack_s_id']);
            $packages['pack_keywords']=implode(",", $packages['pack_keywords']);
            //  return $package;
            if($package->update($packages)){
                 if($package['pack_type']=='event')
                {
                    for($dp=0;$dp<count($package['pack_date']);$dp++)
                    {
                        $eventDatePrice=array();
                        
                        $eventDatePrice['edp_date']=date('Y-m-d',strtotime($package['pack_date'][$dp]));
                        $eventDatePrice['edp_date_price']=$package['pack_date_price'][$dp];
                        $eventDatePrice['edp_pack_id']=$pack_id;
                       
                        $edp_id=EventDateprice::create($eventDatePrice)->edp_id;
                    }
                }
                for ($i=1; $i <=2 ; $i++) { 
                   for($k=0; $k < count($packages['pack_it_sub_title'.$i]) ; $k++)
                    {
                        if(isset($package['pack_it_sub_title'.$i][$k]) && isset($package['pack_it_desc'.$i][$k]))
                        {
                            $middle=($i==1)?'day':'act';
                             $itinery=array();
                            $itinery[$k]['it_'.$middle.'_sub_title']=$packages['pack_it_sub_title'.$i][$k];
                            $itinery[$k]['it_'.$middle.'_desc']=$packages['pack_it_desc'.$i][$k];
                            $itinery[$k]['it_'.$middle.'_pack_id']=$pack_id;
                            $itinery[$k]['it_'.$middle.'_delete']=0;
                            $itinery[$k]=(array)$itinery[$k];
                          if($i==1)
                            {
                                if(isset($packages['pack_it_day_id'.$i.$k]))
                                $it_day_id=ItineraryDay::find($packages['pack_it_day_id'.$i.$k])->it_day_id;
                                else
                                    $it_day_id=0;
                                if($it_day_id>0)
                                {
                                    ItineraryDay::find($packages['pack_it_day_id'.$i.$k])->fill($itinery[$k])->save();
                                }
                                else
                                {
                                    ItineraryDay::create($itinery[$k]);
                                }
                            }
                            else
                            {
                                if(isset($packages['pack_it_act_id'.$i.$k]))
                                 $it_act_id=ItineraryActivity::find($packages['pack_it_act_id'.$i.$k])->it_day_id;
                                    else
                                        $it_act_id=0;
                                if($it_act_id>0)
                                {
                                    ItineraryActivity::find($packages['pack_it_act_id'.$i.$k])->fill($itinery[$k])->save();
                                }
                                else
                                {
                                    ItineraryActivity::create($itinery[$k]);
                                }
                            }
                        }
                    }
                }
                if ($packages['pack_features']!='') {
                    $packageFeature=array();
                    $packageFeature['pf_title'] = $packages['pack_features'];
                    $package->packageFeatures()->update($packageFeature);
                    
                }
                for ($j=0; $j <$packages['pack_total_images'] ; $j++) { 
                        $packageImage=array();
                        if ($r->hasFile('image'.$j)) {
                            
                            
                            if(isset($packages['pack_imageId'.$j]))
                            {
                                $imageId=PackageImage::find($packages['pack_imageId'.$j])->pi_id;
                                if(($imageId)>0)
                                {
                                    list($regular) = $this->uploadFiles($r, $packages['pack_title'], 'image'.$j,[$this->path],[PackageImage::find($packages['pack_imageId'.$j])->pi_img_path]);
                                    $packageImage['pi_img_path'] = $regular;
                                    $packageImage['pi_pack_id'] = $pack_id;
                                    PackageImage::find($packages['pack_imageId'.$j])->fill($packageImage)->save();
                                }   
                                else
                                {
                                    list($regular) = $this->uploadFiles($r, $packages['pack_title'], 'image'.$j,[$this->path]);
                                    $packageImage['pi_img_path'] = $regular;
                                    $packageImage['pi_pack_id'] = $pack_id;
                                    PackageImage::create($packageImage);
                                }
                            }
                            else
                            {
                                list($regular) = $this->uploadFiles($r, $packages['pack_title'], 'image'.$j,[$this->path]);
                                $packageImage['pi_img_path'] = $regular;
                                $packageImage['pi_pack_id'] = $pack_id;
                                PackageImage::create($packageImage);
                            }
                    }
                }
                $this->res['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        
        $package->update(['pack_delete' => 1]);
        $package->itinerary()->update(['it_delete'=>1]);
        $package->packageFeatures()->update(['pf_delete'=>1]);
        $package->packageImages()->update(['pi_delete'=>1]);

        return redirect()->back();
    }

    public function allSubcatOfCat(Request $request)
    {
       $subcategories= \App\Category::find($request['id'])->subCategories()->get();
       $str='';
       foreach ($subcategories as $key => $value) {
           $str.='<option value='.$value->sc_id.'>'.$value->sc_title.'</option>';
       }
       echo $str;exit;
    }

    public function removeItinarydays(Request $request)
    {
        ItineraryDay::find($request->id)->fill(['it_day_delete'=>1])->update();
        echo "success";exit;
    }
    public function removeItinaryacts(Request $request)
    {
        ItineraryActivity::find($request->id)->fill(['it_act_delete'=>1])->update();
        echo "success";exit;
    }
    public function removePackageImage(Request $request)
    {
        PackageImage::find($request->id)->fill(['pi_delete'=>1])->update();
        echo "success";exit;
    }

    public function autocomplete(Request $request)
    {
        $searchkeywords=SearchKeyword::select("sk_title as text","sk_id as value")->where("sk_title","LIKE","%{$request->input('query')}%")->get();
        if(count($searchkeywords)>0)
        {
            return response()->json($searchkeywords);
        }
        else
        {
            $data['sk_title']=$request->input('query');
            $sk_id=SearchKeyword::create($data)->sk_id;
            if($sk_id>0)
            {
                $searchkeywords=SearchKeyword::select("sk_title as text","sk_id as value")->where("sk_title","LIKE","{$request->input('query')}%")->get();
                if(count($searchkeywords)>0)
                {
                    return response()->json($searchkeywords);
                }
            }
        }
    }

    public function getPackageTitleForSearch(Request $request)
    {
        $packageList=Package::take(10)->where('pack_title','LIKE','%'.request('q').'%')->get();
        foreach ($packageList as $key => $value) {
            $json[] = ['id'=>$value['pack_title'], 'text'=>$value['pack_title']];
        }

            echo json_encode($json);
    }
}
