<?php

namespace App\Http\Controllers;

use App\PackageImage;
use Illuminate\Http\Request;

class PackageImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PackageImage  $packageImage
     * @return \Illuminate\Http\Response
     */
    public function show(PackageImage $packageImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageImage  $packageImage
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageImage $packageImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackageImage  $packageImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackageImage $packageImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageImage  $packageImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageImage $packageImage)
    {
        //
    }
}
