<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\Classes\EaseBuzz;
use App\OrderDetail;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    use GetData;    

    protected $MERCHANT_KEY="S9EPJ0XSLC";
    protected $SALT='QRB0IT2YEL';
    protected $ENV='test';
// protected $MERCHANT_KEY="ULEMMSOIKI";
// protected $SALT='0VAB7M9JRK';
    // protected $ENV='prod';



    public function getPay(Request $r){

        $re = $r->all();
        $posted = array();

        if(!empty($re)) {
            foreach($re as $key => $value) {
                $posted[$key] = htmlentities($value, ENT_QUOTES);
                $posted[$key] = trim($value);
            }
        }
        $formError = 0;
        if(sizeof($posted) > 0) {            
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            if(
                empty($posted['amount'])
                || empty($posted['firstname'])
                || empty($posted['email'])
                || empty($posted['phone'])
                || empty($posted['productinfo'])
                || empty($posted['surl'])
                || empty($posted['furl'])
            ) {
                $formError = 1;
            }

            $this->addOrderDetail($r);
            $a = new EaseBuzz();
            return $a->easepay_page(array('key' => $this->MERCHANT_KEY,
                'txnid' => $txnid,
                'amount' => $posted['amount'],
                'firstname' => $posted['firstname'],
                'email' => $posted['email'],
                'phone' => $posted['phone'],
                'udf1' => $posted['udf1'],
                'udf2' => $posted['udf2'],
                'udf3' => $posted['udf3'],
                'udf4' => $posted['udf4'],
                'udf5' => $posted['udf5'],
                'productinfo' => $posted['productinfo'],
                'surl' => $posted['surl'],
                'furl' => $posted['furl']), $this->SALT, $this->ENV);
        }

        return view('users.pay')->with([
            'posted' => $posted,      
            'formError' => $formError,
            'package' => Package::find($id)
        ]);
    }

    public function addOrderDetail($r){
        $userId = $this->addUser($r);

        $orderDetail = OrderDetail::create([
            'order_d_id' => 'WTP' . rand(99999, 4).(OrderDetail::max('order_id')+1),
            'order_user_id' =>$userId, 
            'order_package' => $r->package, 
            'order_status' => 1,
            'order_total' => $r->amount
        ]);                
        $r->session()->put('order_id', $orderDetail->order_id);
    }
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    if (Auth::guest()) {
        return redirect()->route('payment.create');            
    }
    return view('admin.view_payments')->with(['payments' => Payment::get()]);
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create(Request $r)
{
    list($posted, $this->txnid) = $this->listd($r);

    return view('users.payment')->with(['hash' => $this->hash, 'formError' => $this->formError, 'action' => url('placeorder'), 'MERCHANT_KEY' => $this->MERCHANT_KEY, 'txnid' => $this->txnid, 'data' => $r->all()]);
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//
}

/**
* Display the specified resource.
*
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function show(Payment $payment)
{
//
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function edit(Payment $payment)
{
//
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Payment $payment)
{
//
}

/**
* Remove the specified resource from storage.
*
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function destroy(Payment $payment)
{
//
}
public function orderStatus(Request $r){
// $orderId = $r->session()->get('order_id');
    $status = false;
    $orderDetail = OrderDetail::find($r->session()->get('order_id'));
    $ph = Payment::create([
        'ph_order_id' => $orderDetail->order_id,
        'ph_email' => is_null($r->email) ? $orderDetail->order_email : $r->email,
        'ph_phone' => is_null($r->phone) ? $orderDetail->order_mobile : $r->phone,
        'ph_status' => $r->status,
        'ph_txn_id' => $r->txnid,
        'ph_amount' => $r->amount,
        'ph_fullname' => $r->firstname,
        'ph_bank_ref_num' => $r->bank_ref_num,
        'ph_bankcode' => $r->bankcode,
        'ph_unmapped' => $r->unmappedstatus,
        'ph_added_on' => $r->addedon
    ]);
    $ease = new EaseBuzz();
    $chk = $ease->response($r->all(), $this->SALT);
    if ($r->status == 'success') {
        $msg = 'We recieved Rs. '.number_format($ph->ph_amount,2).' on behalf of order '.$orderDetail->order_d_id;
        $this->sendSMS($ph->ph_phone, $msg);
        // $this->sendEmail($r->email, $r->firstname, ['id' => $orderId, 'msg' => $msg], 'notification');
        $status = true;

    }
    return redirect()->route('previous',['id' => $orderDetail->order_id, 'status' => $status]);

}  

public function listd(Request $r)
{

    $posted = array();
    if (!empty($r)) {
        foreach ($r->all() as $key => $value) {
            $posted[$key] = $value;

        }
    }

    if (empty($posted['txnid'])) {
        $this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    } else {
        $this->txnid = $posted['txnid'];
    }

    return [$posted, $this->txnid];
}

public function previous($id, $status = false, Request $r){        
    $order = OrderDetail::find($id);
    return view('users.payment_status')->with(['order' => $order, 'status' => $status]);
}

function getPayView(Request $request)
{
    $packageData=Package::find($request['id']);
    $packageData->pack_cost=$request['price'];
    
    return view('users.pay')->with(['package'=>$packageData]);
}
}
