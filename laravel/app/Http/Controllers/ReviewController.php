<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use App\Package;
use Auth;
use DataTables;
use App\Http\Traits\GetData;

class ReviewController extends Controller
{
     use GetData;

    protected $response = ['msg' => 'error'];

    protected $path = 'images/reviewer/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_reviews');
    }

     public function allReviews()
    {
        if(Auth::user()->type==101)
        {
          $reviews=Review::orderBy('rv_id','DESC');  
        }
        else if(Auth::user()->type==103)
        {
            $reviews=Review::join('packages', function($join) {
              $join->on('reviews.rv_pack_id', '=', 'packages.pack_id');
            })->where('packages.pack_user',Auth::user()->id)->get();
        }
        
        $datatable = DataTables::of($reviews)->addColumn('packagename', function($rev){
            return str_limit((!is_null($packtitle=Package::find($rev->rv_pack_id)->pack_title)?$packtitle:''),30);
        })->addColumn('name', function($rev){
            return str_limit($rev->rv_u_name,30);
        })->addColumn('mobile', function($rev){
            return $rev->rv_u_phone;
        })->addColumn('action', function($rev){
            return '<button class="btn btn-warning btn-xs" onclick = "reviewView('.$rev->rv_id.');"><i class="fa fa-eye"></i> View</button> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.review.destroy', ['id' => $rev->rv_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name', 'mobile','action']);

        /*foreach (['rv_u_name', 'rv_u_email','rv_u_phone','rv_u_photo','rv_created_at','rv_updated_at'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }*/
        $datatable = $datatable->make(true);

        return $datatable;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $reviewer=$request->all();

        if($request->hasFile('pro'))
        {
            
            list($regular) = $this->uploadFiles($request, $reviewer['rv_u_name'], 'pro',[$this->path]);
            $reviewer['rv_u_photo']=$regular;
        }
        
        $rv_id=Review::create($reviewer)->rv_id;
        if(!is_null($rv_id))
        {
            echo "success";exit;
        }
        else
        {
            echo "failure";exit;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $review->delete();
        return redirect()->back();
    }

    public function ajaxViewReview(Request $request)
    {
        $review_details=Review::find($request->input('id'));
        $enquiryStr='';
        $datetime=date('jS M Y h:i:s a',strtotime($review_details->rv_created_at));
        $enquiryStr.='<table  class="table"><thead><tr><th width="60%">Posted Date Time:</th><td width="40%">'.$datetime.'</td></tr><tr><th width="40%">Name</th><th width="40%">Email</th><th width="40%">Mobile</th></tr></thead>';
        $enquiryStr.='<tbody><tr><td>'.$review_details->rv_u_name.'</td><td>'.$review_details->rv_u_email.'</td><td>'.$review_details->rv_u_phone.'</td>
        </tr><tr><td colspan="3"></td></tr><tr><td width="40%" style="font-weight: bold;">Content:</td><td width="100%" colspan="2">'.$review_details->rv_content.'</td></tr></tbody></table>';
       return response()->json($enquiryStr);
    }
}
