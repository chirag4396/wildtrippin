<?php

namespace App\Http\Controllers;

use App\SearchKeyword;
use Illuminate\Http\Request;

class SearchKeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SearchKeyword  $searchKeyword
     * @return \Illuminate\Http\Response
     */
    public function show(SearchKeyword $searchKeyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SearchKeyword  $searchKeyword
     * @return \Illuminate\Http\Response
     */
    public function edit(SearchKeyword $searchKeyword)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SearchKeyword  $searchKeyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SearchKeyword $searchKeyword)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SearchKeyword  $searchKeyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(SearchKeyword $searchKeyword)
    {
        //
    }
}
