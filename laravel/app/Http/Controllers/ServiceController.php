<?php

namespace App\Http\Controllers;

use App\Service;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class ServiceController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/services/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allServices (Request $r){
        $services=Service::where('s_delete',0)->orderBy('s_id','DESC');

        $datatable = DataTables::of($services)->addColumn('name', function($service){
            return str_limit($service->s_title,30);
        })->addColumn('image', function($service){
            return '<img src="'.asset( isset($service->s_img_path) ? $service->s_img_path : 'images/no-image.png').'" class="pro-img">';
        })->addColumn('action', function($service){
            return '<a class="btn btn-info btn-xs" href = "'.route('admin.service.edit', ['id' => $service->s_id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.service.destroy', ['id' => $service->s_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name', 'image', 'action']);

        foreach (['s_title', 's_img_path', 's_delete'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }

    public function index()
    {
        return view('admin.view_services');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.form_layout')->with(['id' => 'service', 'process' => 'add']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
         try {
            $cat = $this->changeKeys('s_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $cat['s_title'], 'image', [$this->path]);
                $cat['s_img_path'] = $regular;
            }
            if(Service::create($cat)){
                $this->res['msg'] = 'success';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.form_layout')->with(['id' => 'service', 'process' => 'edit','record'=>$service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Service $service)
    {
         try {
            $servi = $this->changeKeys('s_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $servi['s_title'], 'image', [$this->path], [$service->s_img_path]);
                $servi['s_img_path'] = $regular;
            }
            if($service->update($servi)){
                $this->res['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
         $service->packages()->update(['pack_delete' => 1]);
        $service->update(['s_delete' => 1]);

        return redirect()->back();
    }
}
