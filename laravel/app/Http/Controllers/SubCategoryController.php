<?php

namespace App\Http\Controllers;

use App\SubCategory;
use App\Category;
use DataTables;
use App\Http\Traits\GetData;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class SubCategoryController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/sub-categories/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function allSubCategories (Request $r){

        $subcategories=SubCategory::where('sc_delete',0)->orderBy('sc_id','DESC');
        $datatable = DataTables::of($subcategories)->addColumn('name', function($scat){
            return str_limit($scat->sc_title,30);
        })->addColumn('image', function($scat){
            return '<img src="'.asset( isset($scat->sc_image) ? $scat->sc_image : 'images/no-image.png').'" class="pro-img">';
        })->addColumn('category', function($scat){
            return str_limit(Category::find($scat->sc_cat_id)->cat_title,30);
        })->addColumn('action', function($scat){
            return '<a class="btn btn-info btn-xs" href = "'.route('admin.sub-category.edit', ['id' => $scat->sc_id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-info btn-xs" onclick = "deleteModal(\''.route('admin.sub-category.destroy', ['id' => $scat->sc_id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
        })->rawColumns(['name', 'image','category', 'action']);

        foreach (['sc_title', 'sc_image', 'sc_delete','sc_cat_id'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }
    public function index(Request $request)
    {
        return view('admin.view_sub_categories');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form_layout')->with(['id' => 'sub-category', 'process' => 'add' , 'categories' => Category::get()]);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $sc = $this->changeKeys('sc_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $sc['sc_title'], 'image', [$this->path]);
                $sc['sc_image'] = $regular;
            }
            if(SubCategory::create($sc)){
                $this->res['msg'] = 'success';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
         return view('admin.form_layout')->with(['id' => 'sub-category', 'process' => 'edit','record'=>SubCategory::find($subCategory->sc_id),'categories' => Category::get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, SubCategory $subCategory)
    {
          try {
            $sc = $this->changeKeys('sc_', $r->all());
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $sc['sc_title'], 'image', [$this->path], [$subCategory->sc_image]);
                $sc['sc_image'] = $regular;
            }
            if($subCategory->update($sc)){
                $this->res['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        //$subCategory->pacakes()->update(['sc_delete' => 1]);
        $subCategory->update(['sc_delete' => 1]);

        return redirect()->back();
    }
}
