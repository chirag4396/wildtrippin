<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Hash;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function allVendors (Request $r){
        $users=User::orderBy('id','DESC');

        $datatable = DataTables::of($users)->addColumn('name', function($user){
            return str_limit($user->name,30);
        })->addColumn('email', function($user){
            return str_limit($user->email,30);
        })->addColumn('type', function($user){
           if($user->type==101)
           {
            return 'Super Admin';
           }
           else
           {
            return 'Vendor';
           }
        })->addColumn('action', function($user){
            if(Auth::user()->id==$user->id && Auth::user()->type==103)
            {
                return '<a class="btn btn-info btn-xs" href = "'.route('admin.user.edit', ['id' => $user->id]).'"><i class="fa fa-pencil"></i> Edit</a>';
            }
            else if(Auth::user()->type==101)
            {
                if(Auth::user()->id==$user->id)
                {
                    return '<a class="btn btn-info btn-xs" href = "'.route('admin.user.edit', ['id' => $user->id]).'"><i class="fa fa-pencil"></i> Edit</a>';
                }
                else{
                    return '<a class="btn btn-info btn-xs" href = "'.route('admin.user.edit', ['id' => $user->id]).'"><i class="fa fa-pencil"></i> Edit</a> | <button class="btn btn-danger btn-xs" onclick = "deleteModal(\''.route('admin.user.destroy', ['id' => $user->id]).'\');"><i class="fa fa-pencil"></i> Delete</button>';
                }
                
            }
            else
            {
                return '';
            }
            
            
        })->rawColumns(['name','email','type','action']);

        foreach (['password'] as $key => $value) {
            $datatable = $datatable->removeColumn($value);
        }
        $datatable = $datatable->make(true);

        return $datatable;


    }
    public function index()
    {
         return view('admin.view_vendors');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form_layout')->with(['id' => 'user', 'process' => 'add']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            
            $user=$request->all();
            $user['password']=Hash::make($user['password']);
            if(User::create($user)){
                $this->res['msg'] = 'success';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.form_layout')->with(['id' => 'user', 'process' => 'edit','record'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try {
           
            if($user->update($user)){
                $this->res['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e->getMessage(); 
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->packages()->update(['pack_delete' => 1]);
        $user->update(['pack_delete' => 1]);

        return redirect()->back();
    }
}
