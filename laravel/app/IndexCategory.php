<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndexCategory extends Model
{
    
	protected $primaryKey = 'ic_id';

	protected $fillable = ['ic_title', 'ic_delete','ic_image','ic_link'];

	public $timestamps = false;
}
