<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{    
	protected $primaryKey = 'it_id';

	protected $fillable = ['it_title', 'it_sub_title', 'it_pack_id', 'it_description','it_delete', 'it_city', 'it_spots'];

	public $timestamps = false;	

	public function package(){
		return $this->belongsTo(\App\Package::class, 'it_pack_id');
	}
}
