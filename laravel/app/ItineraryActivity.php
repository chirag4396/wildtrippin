<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItineraryActivity extends Model
{
    protected $primaryKey = 'it_act_id';

	protected $fillable = ['it_act_sub_title', 'it_act_pack_id', 'it_act_desc','it_act_delete'];

	public $timestamps = false;	

	public function package(){
		return $this->belongsTo(\App\Package::class, 'it_act_pack_id');
	}
}
