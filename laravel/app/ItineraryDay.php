<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItineraryDay extends Model
{
    protected $primaryKey = 'it_day_id';

	protected $fillable = ['it_day_sub_title', 'it_day_pack_id', 'it_day_desc','it_day_delete'];

	public $timestamps = false;	

	public function package(){
		return $this->belongsTo(\App\Package::class, 'it_day_pack_id');
	}
}
