<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
	protected $primaryKey = 'os_id';
	public $timestamps = false;
	public $table = 'order_status';
	protected $fillable = [
		'os_title'
	];

	public function order(){
    	return $this->hasMany(\App\Models\OrderDetail::class, 'order_status');
    }
}
