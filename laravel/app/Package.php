<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $primaryKey = 'pack_id';

    protected $fillable = ['pack_type','pack_cat_id','pack_sc_id','pack_title', 'pack_sub_title', 'pack_description', 'pack_price', 'pack_discount', 'pack_facilities', 'pack_inclusions', 'pack_exclusions','pack_days','pack_nights','pack_city','pack_delete','pack_keywords','pack_s_id','pack_startdate','pack_enddate','pack_top_trand','pack_user'];

    CONST CREATED_AT = 'pack_created_at';
    CONST UPDATED_AT = 'pack_updated_at';

    public function packageImages(){
		return $this->hasMany(\App\PackageImage::class, 'pi_pack_id');
	}
	public function packageFeatures(){
		return $this->hasOne(\App\PackageFeature::class, 'pf_pack_id');
	}
	public function itinerary(){
		return $this->hasMany(\App\Itinerary::class, 'it_pack_id');
	}
	public function itinerarydays(){
		return $this->hasMany(\App\ItineraryDay::class, 'it_day_pack_id');
	}
	public function itineraryctivities(){
		return $this->hasMany(\App\ItineraryActivity::class, 'it_act_pack_id');
	}
	public function subCategories(){
		return $this->belongsToMany(\App\SubCategory::class);
	}
	public function category(){
		return $this->belongsTo(\App\Category::class,'pack_cat_id');
	}
	public function service(){
		return $this->belongsTo(\App\Service::class,'pack_s_id');
	}
	public function enquiry(){
		return $this->hasMany(\App\Enquiry::class, 'eq_pack_id');
	}
	public function reviews(){
		return $this->hasMany(\App\Review::class, 'rv_pack_id');
	}
}
