<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageFeature extends Model
{
    
    protected $primaryKey = 'pf_id';

    protected $fillable = ['pf_title','pf_pack_id','pf_delete'];

    CONST CREATED_AT = 'pf_created_at';
    CONST UPDATED_AT = 'pf_updated_at';

    public function package(){
		return $this->belongsTo(\App\Package::class, 'pf_pack_id');
	}
}
