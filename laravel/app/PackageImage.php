<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageImage extends Model
{    
    protected $primaryKey = 'pi_id';

	protected $fillable = ['pi_img_path', 'pi_pack_id','pi_delete'];

	public $timestamps = false;	

	public function package(){
		return $this->belongsTo(\App\Package::class, 'pi_pack_id');
	}
}
