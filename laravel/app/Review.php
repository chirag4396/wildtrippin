<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $primaryKey = 'rv_id';
	
	protected $fillable = ['rv_pack_id','rv_u_name','rv_u_email','rv_u_phone','rv_u_photo','rv_content'];

    CONST CREATED_AT = 'rv_created_at';
    CONST UPDATED_AT = 'rv_updated_at';

    public function package(){
		return $this->belongsTo(\App\Pakcage::class,'rv_pack_id');
	}
}
