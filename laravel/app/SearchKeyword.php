<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchKeyword extends Model
{
     protected $primaryKey = 'sk_id';

    protected $fillable = ['sk_title'];

    CONST CREATED_AT = 'sk_created_at';
    CONST UPDATED_AT = 'sk_updated_at';
}
