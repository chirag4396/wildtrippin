<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $primaryKey = 's_id';

	protected $fillable = ['s_title', 's_delete','s_img_path','s_desc'];

	CONST CREATED_AT = 's_created_at';
    CONST UPDATED_AT = 's_updated_at';

	public function packages(){
		return $this->hasMany(\App\Package::class, 'pack_s_id');
	}
}
