<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	protected $primaryKey = 'sc_id';

	protected $fillable = ['sc_title', 'sc_cat_id', 'sc_delete','sc_image'];

	public $timestamps = false;

	public function category(){
		return $this->belongsTo(\App\Category::class, 'sc_cat_id');
	}
	public function packages(){
		return $this->hasMany(\App\Package::class, 'pack_sc_id');
	}
}
