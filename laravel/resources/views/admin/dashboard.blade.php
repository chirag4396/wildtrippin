@extends('admin.layouts.master')

@section('content')
<div class="right_col" role="main">
	<div class="row top_tiles">
		<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-check-square-o"></i></div>
				<div class="count">179</div>
				<h3>New Sign ups</h3>
				<p>Lorem ipsum psdea itgum rixt.</p>
			</div>
		</div>
		<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-check-square-o"></i></div>
				<div class="count">179</div>
				<h3>New Sign ups</h3>
				<p>Lorem ipsum psdea itgum rixt.</p>
			</div>
		</div>		
		<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-check-square-o"></i></div>
				<div class="count">179</div>
				<h3>New Sign ups</h3>
				<p>Lorem ipsum psdea itgum rixt.</p>
			</div>
		</div>
	</div>
</div>
@endsection