@extends('admin.layouts.master')
@php
	$title = $process == 'add' ? 'Create new '.ucwords(str_replace('-', ' ', $id)) : 'Edit '.ucwords(str_replace('-', ' ', $id));
@endphp
@section('title')
{{ $title }}
@endsection

@push('header')
<script>
	var ID = '{{ $id }}';
</script>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3>{{ $title }}</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$id.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					@include('admin.forms.'.str_replace('-', '_', $id).'_form', ['id' => $id , 'record' => ($process == 'edit' ? $record : null )])
				</div>
			</div>
		</div>
	</div>
</div>
@endsection