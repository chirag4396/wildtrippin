@push('header')
@php
$ID = 'country';
@endphp
<script>
	ID = '{{ $ID }}';
</script>
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left">
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Country Name
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "name" data-validate = "empty">
		</div>
	</div>
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">Add
		</button>							
	</div>					
</form>