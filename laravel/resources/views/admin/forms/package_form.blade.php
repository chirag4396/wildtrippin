@push('header')
<link href="{{ asset('/admin-assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/tokenize/tokenize2.css') }}" rel="stylesheet" />

<style type="text/css">
.buttonFinish 
{
	display: none;
}
</style>


@endpush

<form id = "{{ $id }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data" >
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">

				<div class="x_content">


					<!-- Smart Wizard -->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li>
								<a href="#step-1">
									<span class="step_no">1</span>
									<span class="step_descr">
										Step 1<br />
										<small>Package Basic Info</small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-2">
									<span class="step_no">2</span>
									<span class="step_descr">
										Step 2<br />
										<small>Package Images</small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-3">
									<span class="step_no">3</span>
									<span class="step_descr">
										Step 3<br />
										<small>Package Itinerary Days</small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-4">
									<span class="step_no">4</span>
									<span class="step_descr">
										Step 4<br />
										<small>Package Itinerary Activities</small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-5">
									<span class="step_no">5</span>
									<span class="step_descr">
										Step 5<br />
										<small>Package Features</small>
									</span>
								</a>
							</li>
						</ul>
						@include('admin.includes.package_step_1', ['record' => $record, 'process' => $process])
						<div class="Step1NextButton" id="step1NextButton" style="float: right;">
							<a class="btn btn-success" id = "step1NxtBtn">Next</a>
						</div>
						@include('admin.includes.package_step_2', ['record' => $record, 'process' => $process])
						@include('admin.includes.package_step_3', ['record' => $record, 'process' => $process])
						@include('admin.includes.package_step_4', ['record' => $record, 'process' => $process])
						@include('admin.includes.package_step_5', ['record' => $record, 'process' => $process])
					</div>
				</div>
			</div>
		</div>
	</div>

	{{-- <div class="form-group" style="float: right">							
		<button type="submit" class="btn btn-success">{{ ($process=='edit')?'Update' :'ADD' }}</button>	

	</div>	 --}}
</form>

@push('footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<script src="{{ asset('admin-assets/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">
	@php
	$url = ($process == 'edit') ? 'admin.'.$id.'.index' : 'admin.'.$id.'.store';
	@endphp
	var actnext = 1;
	$(".add-more2").click(function(e){
		e.preventDefault();
		var addto = "#fieldact" + actnext;
		var addRemove = "#fieldact" + (actnext);
		actnext = actnext + 1;
		var newIn = '<input autocomplete="off" class="form-control col-md-6 col-xs-12" id="fieldact' + actnext + '" name="it_sub_title2[]" type="text">';
		var newInput = $(newIn);
		var removeBtn = '<button id="remove' + (actnext - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="fieldact">';
		var removeButton = $(removeBtn);
		$(addto).after(newInput);
		$(addRemove).after(removeButton);
		$("#fieldact" + actnext).attr('data-source',$(addto).attr('data-source'));
		$("#count").val(actnext);  

		$('.remove-me').click(function(e){
			e.preventDefault();
			var fieldactNum = this.id.charAt(this.id.length-1);
			var fieldactID = "#fieldact" + fieldactNum;
			$(this).remove();
			$(fieldactID).remove();
		});
	});

	$(document).ready(function () {
		//'<button class="btn btn-success" id = "step1NxtBtn">Next</button>'
					
		$('.buttonNext').addClass('hidden');
		$('#step1NxtBtn').click(function () {
			//$('.buttonNext').removeClass('hidden');
			
			var validationMessage = '';
			var validationMessage1 = '';
			var validationMessage2 = '';
			var validationMessage3 = '';
			var focusSet = false;
			var focusSet1 = false;
			var focusSet2 = false;
			$.each($(this).closest('.form_wizard ').find('input[type="text"]'), function () {
				if ($(this).val() == '' && $(this).attr('id')!='it_sub_title1' && $(this).attr('id')!='it_sub_title2')
				{
					validationMessage = "Please fill " + $(this).closest('.form-group').find('label').html().replace('*', '') + "\n";
					$((this)).after("<span class='validation' style='color:red;'>"+validationMessage+"</span>");
					validationMessage='';
					focusSet = true;
				} 
				else
				{
					//focusSet = false;
					$(this).next("span").remove();
				}


			});
			$.each($(this).closest('.form_wizard ').find('input[type="number"]'), function () {
				alert($(this).val());
				if ($(this).val() == '')
				{
					validationMessage1 = "Please fill " + $(this).closest('.form-group').find('label').html().replace('*', '') + "\n";
					$((this)).after("<span class='validation' style='color:red;'></span>");
					validationMessage1='';
					focusSet1 = true;
				}
				else if($.isNumeric($(this).val())==false) 
				{
					validationMessage2 = "Please sdfsdf " + $(this).closest('.form-group').find('label').html().replace('*', '') + "\n";
					$((this)).after("<span class='validation' style='color:red;'></span>");
					validationMessage2='';
					focusSet1 = true;
				}
				else
				{
					//focusSet = false;
					$(this).next("span").remove();
				}


			});
			$.each($(this).closest('.form_wizard ').find('select'), function () {
				
				if ($(this).val() == -1)
				{
					validationMessage3 = "Please choose" + $(this).closest('.form-group').find('label').html().replace('*', '') + "\n";
					$((this)).after("<span class='validation' style='color:red;'>"+validationMessage3+"</span>");
					validationMessage3='';
					focusSet2 = true;
				} 
				else
				{
					//focusSet = false;
					$(this).next("span").remove();
				}


			});
	           console.log(focusSet+'------'+focusSet1+'-----'+focusSet2);
	          if(focusSet==true || focusSet1==true || focusSet2==true){
	          	
	          } 
	          if(focusSet==false)
	          {
	          	$('#step1NextButton').hide();
	          	$('.buttonNext').removeClass('hidden');
	          	 $('.buttonNext').click();
	          }

	      
	    });

	});
	$('#{{ $id }}Form').CRUD({
		url : '{{ route($url) }}',
		processResponse : function (data) {
			//console.log(data);return false;
			if(data.pack_type == 'package'){
				
				window.location='{{ route('admin.package.index') }}';
			}
			if(data.pack_type == 'event'){
				
				window.location='{{ route('admin.indexevents') }}';
			}
			
		}
	});		

</script>

@endpush