<form id = "{{ $id }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "name" maxlength = "255" data-validate = "empty|alphaSpace" value="{{ $record->name or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "email" maxlength = "255" data-validate = "empty" value="{{ $record->email or '' }}" {{ ($process=='edit')? 'disabled':'' }}>
		</div>
	</div>
	@if(Auth::user()->id==101)
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Admin Type</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select name="type" class="form-control col-md-7 col-xs-12">
				<option value="101">Super Admin</option>
				<option value="103">Vendor</option>
			</select>
		</div>
	</div>
	@endif
	@if($process=='add')
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="password" class="form-control col-md-7 col-xs-12" name = "password" maxlength = "255" data-validate = "empty" value="{{ $record->password or '' }}">
		</div>
	</div>
	@endif
	{{-- <div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Display Picture</label>		
		<div class="col-md-3 col-sm-3 col-xs-12">			
			<div class="clearfix"></div>		
			<div class="text-center">
				<input type = "file" id ="image" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "image">	
				<label id = "imagePreview" for = "image" style="background-image: url('{{ asset((isset($record['cat_image']) )? $record['cat_image'] : '/images/no-image.png') }}');">Choose Image</label>
			</div>
		</div>
	</div>	 --}}				
	<div class="ln_solid">
	</div>
	@if (!is_null($record))
	<input type="hidden" name="id" value="{{ $record->name }}">
	@endif
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ ($process=='edit')?'Update' :'ADD' }}
		</button>							
	</div>					
</form>	

@push('footer')
<script>
	@php
	$url = ($process == 'edit') ? 'admin.'.$id.'.index' : 'admin.'.$id.'.store';
	@endphp
	$('#{{ $id }}Form').CRUD({

		url : '{{ route($url) }}',
		processResponse : function (data) {
			if(data.msg == 'success'){
				$('#imagePreview').css({
					'background-image': 'url("{{ asset('/images/no-image.png') }}")'
				});
				window.location='{{ route('admin.user.index') }}';
			}
			if(data.msg == 'successU'){
				$('#imagePreview').css({
					'background-image': 'url("{{ asset('/images/no-image.png') }}")'
				});
				window.location='{{ route('admin.user.index') }}';
			}
		}
	});		
	imageUpload('image');
</script>
@endpush