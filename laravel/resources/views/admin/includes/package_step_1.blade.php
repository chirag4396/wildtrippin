<div id="step-1">
	<div class="form-group">

		<label class="control-label col-md-4 col-sm-4 col-xs-12">Package Type
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control" name="type" id="type">
				<option value="-1">--Select Type--</option>
				<option value="event" {{ ($record['pack_type']=='event')?'selected':'' }}>Event</option>
				<option value="package"  {{ ($record['pack_type']=='package')?'selected':'' }}>Package</option>
			</select>
		</div>
	</div>
	<div class="form-group">

		<label class="control-label col-md-4 col-sm-4 col-xs-12">Package Category
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control" name="cat_id" id="cat_id">
				<option value="-1">--Select Category--</option>
				@forelse(\App\Category::where('cat_delete',0)->get() as $catvalue)
				<option value="{{ $catvalue->cat_id }}" @if($record['pack_cat_id']==$catvalue->cat_id) selected='selected'  @endif>{{ $catvalue->cat_title }}</option>
				@empty

				@endforelse
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Package SubCategory
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control" multiple="" id="sc_id" name="sc_id[]">
				<option value="-1" selected="">--Select Sub Category--</option>
				@if($process=='edit')
				@forelse(\App\SubCategory::where('sc_delete',0)->get() as $subcatvalue)
				<option value="{{ $subcatvalue->sc_id }}" @if(in_array($subcatvalue->sc_id, explode("|", $record['pack_sc_id']))) selected='selected'  @endif>{{ $subcatvalue->sc_title }}</option>
				@empty

				@endforelse
				@endif
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Package Services
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control" multiple="" id="s_id" name="s_id[]">
				<option value="-1" selected="">--Select Services--</option>
				
				@forelse(\App\Service::where('s_delete',0)->get() as $svalue)
				<option value="{{ $svalue->s_id }}" @if(in_array($svalue->s_id, explode("|", $record['pack_s_id']))) selected='selected'  @endif>{{ $svalue->s_title }}</option>
				@empty

				@endforelse
				
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Package Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "title"  value="{{ $record->pack_title or '' }}" id="pack_title">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Package Sub-title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "sub_title"  value="{{ $record->pack_sub_title or '' }}" id="pack_sutitle">
		</div>
	</div>
	<div class="form-group priceForPackage" style="display: none">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Price
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "price"  value="{{ $record->pack_price or '' }}" id="p_price">


			<span class="fa fa-rupee form-control-feedback right" aria-hidden="true"></span>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Discount
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "discount"  value="{{ $record->pack_discount or '' }}" id="pack_discount">

			<span class="fa fa-rupee form-control-feedback right" aria-hidden="true"></span>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Days
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" class="form-control col-md-7 col-xs-12" name = "days"  value="{{ $record->pack_days or '' }}" min="0" id="pack_days">


		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Night
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" class="form-control col-md-7 col-xs-12" name = "nights"  value="{{ $record->pack_nights or '' }}" min="0" id="pack_night">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Start Date
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class='input-group date'> 
			<input type='text' class="form-control col-md-7 col-xs-12 startdate" id="startdate" name="startdate" value="{{ (isset($record->pack_startdate))?$record->pack_startdate :'' }}" default-value="{{ (isset($record->pack_startdate))?$record->pack_startdate :'' }}" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
			</div>

		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	End Date
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class='input-group date'> 
			<input type='text' class="form-control col-md-7 col-xs-12 enddate" id="enddate" name="enddate" value="{{ (isset($record->pack_enddate))? $record->pack_enddate :'' }}" default-value="{{ (isset($record->pack_enddate))? $record->pack_enddate :'' }}" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
			</div>

		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Include Facilities
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<select class="form-control" multiple name="facilities[]" id="facilities">
				<option value="-1" selected="">--Select included facilities--</option>
				@forelse(\App\Facility::where('facility_status',0)->get() as $fv)
				<option value="{{ $fv->facility_id }}" @if(in_array($fv->facility_id, explode("|", $record['pack_facilities']))) selected='selected'  @endif>{{ $fv->facility_name }}</option>
				@empty
				@endforelse
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12"> Search Keywords
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="tokenize-callable-demo1 form-control" multiple name="keywords[]" id="keywords">
				@forelse(\App\SearchKeyword::get() as $skk=>$skv)
				<option value="{{ $skv->sk_id }}" @if(in_array($skv->sk_id, explode(",", $record['pack_keywords']))) selected='selected' @endif>{{ $skv->sk_title }}</option>
				@empty
				@endforelse
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Select Country
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<select  name="country_id" id="country_id" class="form-control" onchange="getState()">
				<option value="-1" selected="">--Select Country---</option>
				@forelse(\App\Country::get() as $c=>$cdes)
				<option value="{{ $cdes->country_id }}" @if($process=='edit'){{ ( $cdes->country_id ==$record['pack_country'])?'selected':'' }}@endif>{{ $cdes->country_name }}</option>
				@empty
				@endforelse
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Select State
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<select name="state_id" class="form-control" id="state_id" onchange="getCity()">
				<option value="-1" selected="">--Select State---</option>
				@if($process=='edit')
				@foreach(\App\State::get() as $s=>$sdes)
				<option value="{{ $sdes->state_id }}" {{ ( $sdes->state_id ==$record['pack_state'])?'selected':'' }}>{{ $sdes->state_name }}</option>
				@endforeach
				@endif
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">	Select City
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<select name="city" class="form-control" id="city_id">
				<option value="-1" selected="">--Select City--</option>
				@if($process=='edit')
				@foreach(\App\City::get() as $s=>$csdes)
				<option value="{{ $csdes->city_id }}"  {{ ( $csdes->city_id ==$record['pack_city'])?'selected':'' }}>{{ $csdes->city_name }}</option>
				@endforeach
				@endif
			</select>
		</div>
	</div>
	@php
		$countFor=0
	@endphp
	@forelse(\App\EventDateprice::where('edp_pack_id',$record['pack_id'])->get() as $edk=>$edv)
		<div class="form-group myDatepicker2" style="display: none;">
		
			<label class="control-label col-md-4 col-sm-4 col-xs-12">	Select Date and And Their Price
			</label>
			<div class="col-md-3 col-sm-6 col-xs-12" >
				<div class='input-group date' id='myDatepicker2{{ $edk }}'>
					<input type='text' class="form-control col-md-7 col-xs-12" name="date[]" value="{{ $edv->edp_date }}" default-value="{{ $edv->edp_date }}" id="date{{ $edk }}" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12" >
				<div class='input-group date' >
					<input type="text" class="form-control col-md-7 col-xs-12" name = "date_price[]" value="{{ $edv->edp_date_price }}" id="date_price{{ $edk }}">
					<span class="fa fa-rupee form-control-feedback right" aria-hidden="true" ></span>
				</div>
			</div>
			@php
				$countFor++
			@endphp
			@if($edk==0)
			<div class="col-md-2 col-sm-6 col-xs-12">
				<button class="btn btn-warning addmore_date_price" type="button">+</button>
				<button class="btn btn-danger remove_date_price" type="button">-</button>
			</div>
			@endif
	</div>
		@empty
		<div class="form-group myDatepicker2" style="display: none;">
		
			<label class="control-label col-md-4 col-sm-4 col-xs-12">	Select Date and And Their Price
			</label>
			<div class="col-md-3 col-sm-6 col-xs-12" >
				<div class='input-group date' id='myDatepicker20'>
					<input type='text' class="form-control col-md-7 col-xs-12" name="date[]" id="date"/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12" >
				<div class='input-group date' id='myDatepicker2'>
					<input type="text" class="form-control col-md-7 col-xs-12" name = "date_price[]" id="date_price">
					<span class="fa fa-rupee form-control-feedback right" aria-hidden="true" ></span>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<button class="btn btn-warning addmore_date_price" type="button">+</button>
				<button class="btn btn-danger remove_date_price" type="button">-</button>
			</div>
	</div>
		@endforelse

	<div class="addmoreDatePrice"></div>
</div>

@push('footer')
	<script src="{{ asset('admin-assets/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('admin-assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

	<script src="{{ asset('admin-assets/tokenize//tokenize2.js') }}"></script>
	<script type="text/javascript">
		$('#startdate').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$('#enddate').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$('.tokenize-callable-demo1').tokenize2({
			dataSource: function(search, object){
				var path = "{{ URL::to('/autocomplete') }}";
				$.ajax(path, {
					data: { query: search, start: 1 },
					dataType: 'json',
					success: function(data){
						var $items = [];
						$.each(data, function(k, v){
							$items.push(v);
						});
						object.trigger('tokenize:dropdown:fill', [$items]);
					}
				});
			}
		});
		@if($process=='edit')
		@if($record['pack_type']=='event')
		$('.myDatepicker2').show();
		$('.priceForPackage').hide();
		$('#myDatepicker2').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		@else
		$('.priceForPackage').show();
		$('.myDatepicker2').hide();
		@endif
		@endif
		$('#type').change(function(){

			if(this.value=='event')
			{
				$('.myDatepicker2').show();
				$('.priceForPackage').hide();
			}
			else
			{
				$('.priceForPackage').show();
				$('.myDatepicker2').hide();
			}
		});
		$('#myDatepicker20').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$( "#cat_id" ).change(function() {
			$.ajax({url: "{{ route('admin.all-subcatofcat') }}",
				type:'POST',
				data:{id:this.value,
					"_token": "{{ csrf_token() }}"},
					success: function(data){

						$('#sc_id').html(data);
					},
					error: function(data){
						console.log(data);
					}

				});
		});
		function getState()
		{
			var countryId=$('#country_id').val();
			var url="{{ URL::to('/getstate') }}"+'/'+countryId;
			$.ajax({
				url : url ,
				type:'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success:function(data){
					$('#state_id').html(data);
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
		function getCity()
		{
			var stateId=$('#state_id').val();

			var url="{{ URL::to('/getcity') }}"+'/'+stateId;

			$.ajax({
				url : url ,
				type:'post',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success:function(data){
					$('#city_id').html(data);
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
		var datepricenext={{ ($countFor==0)?1:$countFor }};
		$('.addmore_date_price').click(function(e){
			$('.addmoreDatePrice').append('<div class="form-group myDatepicker2"><label class="control-label col-md-4 col-sm-4 col-xs-12">	Select Date and And Their Price</label><div class="col-md-3 col-sm-6 col-xs-12" ><div class="input-group date" id="myDatepicker2'+datepricenext+'"><input type="text" class="form-control col-md-7 col-xs-12" name="date[]"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div></div><div class="col-md-3 col-sm-6 col-xs-12" ><div class="input-group date" id="myDatepicker2'+datepricenext+'"><input type="text" class="form-control col-md-7 col-xs-12" name = "date_price[]" ><span class="fa fa-rupee form-control-feedback right" aria-hidden="true"></span></div></div>');

			$('#myDatepicker2'+datepricenext).datetimepicker({
				format: 'YYYY-MM-DD'
			});
			datepricenext++;
		});
		$('.remove_date_price').click(function(e){
			--datepricenext;
			$(".addmoreDatePrice").children().last().remove();  

		});	
		
	</script>
@endpush