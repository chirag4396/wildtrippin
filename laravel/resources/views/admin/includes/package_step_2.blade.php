<div id="step-2">
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Is in Top Trand?
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control" name="top_trand">
				<option value="-1">--Select Status--</option>
				<option value="1" @if(!is_null($record['pack_top_trand'])==1)  selected='selected' @endif>Enable</option>
				<option value="0" @if(!is_null($record['pack_top_trand'])==0)  selected='selected' @endif>Disable</option>
			</select>
		</div>
	</div>
	<div class="form-group" style="margin-top:30px !important">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<button class="btn btn-warning addimage" type="button">Add More</button>
			<button class="btn btn-danger removeimage" type="button">Remove</button>
		</div>

	</div>
	<input type="hidden" name="total_images" id="total_images">
	@if($process=='edit')
	@php
	$record['images']=\App\PackageImage::where('pi_pack_id',$record['pack_id'])->where('pi_delete',0)->get();
	@endphp
	@forelse($record['images'] as $imgcount => $pi)
	<div class="form-group">
		<input type="hidden" name="imageId{{ $imgcount }}" value="{{ $pi['pi_id'] }}" id="imageId{{ $imgcount }}">
		<label class="control-label col-md-3 col-sm-3 col-xs-12 imageId{{ $imgcount }}">Display Picture </label>		
		<div class="col-md-3 col-sm-3 col-xs-12">			
			<div class="clearfix"></div>		
			<div class="text-center">
				<input type = "file" id ="image{{ $imgcount }}" accept="image/png, image/jpeg, image/jpg" class="hidden form-control" name = "image{{ $imgcount }}">	
				<label id = "image{{$imgcount }}Preview" for = "image{{ $imgcount }}" style="background-image: url('{{ asset(($pi['pi_img_path']!='')? $pi['pi_img_path'] :'/images/no-image.png') }}');">Choose Image</label>
			</div>
		</div>
	</div>
	
	
	@empty
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Display Picture</label>		
		<div class="col-md-3 col-sm-3 col-xs-12">			
			<div class="clearfix"></div>		
			<div class="text-center">
				<input type = "file" id ="image0" accept="image/png, image/jpeg, image/jpg" class="hidden form-control" name = "image0">	
				<label id = "image0Preview" for = "image0" style="background-image: url('{{ asset('/images/no-image.png') }}');">Choose Image</label>
			</div>
		</div>
	</div>
	@endforelse
	@else
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Display Picture</label>		
		<div class="col-md-3 col-sm-3 col-xs-12">			
			<div class="clearfix"></div>		
			<div class="text-center">
				<input type = "file" id ="image0" accept="image/png, image/jpeg, image/jpg" class="hidden form-control" name = "image0">	
				<label id = "image0Preview" for = "image0" style="background-image: url('{{ asset('/images/no-image.png') }}');">Choose Image</label>
			</div>
		</div>
	</div>
	@endif
	<div class="ln_solid"></div>
</div>
@push('footer')
<script type="text/javascript">
	if('{{ $process }}'=='add')
		{
			imageUpload('image0');
		}
		@php
		$countforimage = (($process =='add')? 1 : count($record['images']));
		@endphp
		var imagecount={{ $countforimage }};
		$(".addimage").click(function(e){

			var url='{{ asset('/images/no-image.png') }}';
			$('#step-2').append('<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">Display Picture</label><div class="col-md-3 col-sm-3 col-xs-12"><div class="clearfix"></div><div class="text-center"><input type = "file" id ="image'+(imagecount)+'" accept="image/png, image/jpeg, image/jpg" class="hidden form-control" name = "image'+imagecount+'"><label id = "image'+imagecount+'Preview" for = "image'+imagecount+'" style="background-image: url("'+url+'");">Choose Image</label></div></div></div><div class="ln_solid"></div>');
			imageUpload('image'+imagecount);
			
			imagecount++;
			$('#total_images').val(imagecount);
		});
		countimages=@if($process=='edit'){{ count($record['images']) }}@else 0 @endif;
		$(".removeimage").click(function(e){
			--imagecount;
			if(imagecount==(--countimages))
			{

				var id=$('#imageId'+imagecount).val();
				removeImages(id);
			}
			$("#step-2").children().last().remove();  
			$("#step-2").children().last().remove();  
			$('#total_images').val(imagecount);
		});
		$('#total_images').val(imagecount);

		
		@if($process=='edit')
		@forelse($record['images'] as $imgcount => $pi)
		imageUpload('image{{ $imgcount }}')

		@empty
		imageUpload('image0')
		@endforelse
		@endif
		function removeImages(id)
		{
			var id=id;
			var ajaxURL="{{ URL::to('/admin/remove-package-image') }}";

			$.ajax({
				url:ajaxURL,
				type:'post',
				data:{id:id},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success:function(data){
					$('input:hidden#imageId'+itinerayact).remove();
					$('.imageId'+itinerayact).remove();
				},
				error:function(data){
					console.log(data);
				}
			});
			return;
		}
</script>
@endpush