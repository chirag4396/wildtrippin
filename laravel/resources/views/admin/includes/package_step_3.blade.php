<div id="step-3">
	
	<input type="hidden" name="total_itinerary" id="total_itinerary">
	>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<button class="btn btn-warning additinerarydays" type="button">Add More</button>
			<button class="btn btn-danger removeitinerarydays" type="button">Remove</button>
		</div>

	</div>
	
	@if($process=='edit' && count($record['itinerarydays'])>0)
	@php
	$record['itinerarydays']=\App\ItineraryDay::where('it_day_pack_id',$record['pack_id'])->where('it_day_delete',0)->get();
	@endphp
	@forelse($record['itinerarydays'] as $idk=>$idv)
	<input type="hidden" id="it_day_id1{{ $idk }}" name="it_day_id1{{ $idk }}" value="{{ $idv->it_day_id }}">
	<div class="it_day_1{{ $idk }}">
		<div class="form-group">
			<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title1[]" value="{{ $idv['it_day_sub_title'] }}">

			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<textarea class="form-control ckeditor" id="it_desc1{{ $idk }}" name="it_desc1[]">{{ $idv['it_day_desc'] }}</textarea>
			</div>
		</div>
	</div>
	@empty
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title1[]" id="it_sub_title1">

		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control ckeditor" id="it_desc1" name="it_desc1[]" id="it_desc1"></textarea>
		</div>
	</div>
	@endforelse
	@else
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title1[]" id="it_sub_title1">

		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control ckeditor" id="it_desc1" name="it_desc1[]" id="it_desc1"></textarea>
		</div>
	</div>
	@endif
	
	<div class="addmoreotenarydays_div"></div>
</div>
@push('footer')
<script type="text/javascript">
	var itinerayday={{ ($process=='edit' && count($record['itinerarydays'])>0)?count($record['itinerarydays']):1 }};
		$('.additinerarydays').click(function(e){
			$('.addmoreotenarydays_div').append('<div class="form-group"><label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title</label><div class="col-md-6 col-sm-6 col-xs-12"><input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title1[]"></div></div><div class="form-group"><label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description</label><div class="col-md-6 col-sm-6 col-xs-12"><textarea class="form-control ckeditor" id="it_desc1'+(itinerayday)+'" name="it_desc1[]"></textarea></div></div>');
			CKEDITOR.replace("it_desc1"+itinerayday);	
			++itinerayday;
		});
		
		countday={{ ($process=='edit' && count($record['itinerarydays'])>0)?count($record['itinerarydays']):1 }};
		$(".removeitinerarydays").click(function(e){
			--itinerayday;
			
			if(itinerayday==(--countday))
			{

				var id=$('#it_day_id1'+(itinerayday)).val();
				removeDayItinerary(id);
			}
			$(".addmoreotenarydays_div").children().last().remove();  
			$(".addmoreotenarydays_div").children().last().remove();  
			
			
		});
		
		@if($process=='edit' && count($record['itinerarydays'])>0)
		@forelse($record['itinerarydays'] as $idk=>$idv)
		CKEDITOR.replace("it_desc1"+{{ $idk }});
		@empty
		@endforelse
		@endif
		

		function removeDayItinerary(id)
		{
			var id=id;
			var ajaxURL="{{ URL::to('/admin/remove-day-itinerary') }}";

			$.ajax({
				url:ajaxURL,
				type:'post',
				data:{id:id},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success:function(data){
	              //  $('#it_desc1'+itinerayday).hide();

	              $('input:hidden#it_day_id1'+itinerayday).remove();
	              $('.it_day_1'+itinerayday).remove();


	          }
	      });
			return;
		}
		
</script>
@endpush