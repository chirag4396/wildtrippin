<div id="step-4">
	<input type="hidden" name="total_itinerary" id="total_itinerary">
	>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<button class="btn btn-warning additineraryact" type="button">Add More</button>
			<button class="btn btn-danger removeitineraryact" type="button">Remove</button>
		</div>

	</div>
	@if($process=='edit')
	@php
	$record['itinerayact']=\App\ItineraryActivity::where('it_act_pack_id',$record['pack_id'])->where('it_act_delete',0)->get();
	@endphp
	@forelse($record['itinerayact'] as $iak=>$iav)
	<input type="hidden" name="it_act_id2{{ $iak }}" value="{{ $iav->it_act_id }}" id="it_act_id2{{ $iak }}">
	<div class="it_act_2{{ $iak }}">
		<div class="form-group">
			<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title2[]" value="{{ $iav->it_act_sub_title }}" >

			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<textarea class="form-control ckeditor" id="it_desc2{{ $iak }}" name="it_desc2[]">{{ $iav->it_act_desc }}</textarea>
			</div>
		</div>
	</div>
	@empty
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title2[]" id="it_sub_title2">

		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control ckeditor" id="it_desc2" name="it_desc2[]" id="it_desc2"></textarea>
		</div>
	</div>
	@endforelse
	@else
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title2[]" id="it_sub_title2">

		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control ckeditor" id="it_desc2" name="it_desc2[]" id="it_desc2"></textarea>
		</div>
	</div>
	@endif
	<div class="addmoreotenaryact_div"></div>
</div>
@push('footer')
<script type="text/javascript">
	var itinerayact={{ ($process=='edit' && count($record['itinerayact'])>0)?count($record['itinerayact']):1 }};
	$('.additineraryact').click(function(e){
		$('.addmoreotenaryact_div').append('<div class="form-group"><label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Title</label><div class="col-md-6 col-sm-6 col-xs-12"><input type="text" class="form-control col-md-7 col-xs-12" name = "it_sub_title2[]"></div></div><div class="form-group"><label class="control-label col-md-4 col-sm-4 col-xs-12">Itineraries Description</label><div class="col-md-6 col-sm-6 col-xs-12"><textarea class="form-control ckeditor" id="it_desc2'+(itinerayact)+'" name="it_desc2[]"></textarea></div></div>');
		CKEDITOR.replace("it_desc2"+itinerayact);	++itinerayact;
	});
	actcount={{ ($process=='edit' && count($record['itinerayact'])>0)?count($record['itinerayact']):1 }};
	$(".removeitineraryact").click(function(e){
		--itinerayact;

		if(itinerayact==(--actcount))
		{
			alert(itinerayact);
			var id=$('#it_act_id2'+itinerayact).val();
			removeActItinerary(id);
		}
		$(".addmoreotenaryact_div").children().last().remove();  
		$(".addmoreotenaryact_div").children().last().remove();  
		
		
	});
	@if($process=='edit' && count($record['itinerayact'])>0)
	@forelse($record['itinerayact'] as $iak=>$iav)
	CKEDITOR.replace("it_desc2"+{{ $iak }});
	@empty
	@endforelse
	@endif
	function removeActItinerary(id)
	{
		var id=id;
		var ajaxURL="{{ URL::to('/admin/remove-act-itinerary') }}";

		$.ajax({
			url:ajaxURL,
			type:'post',
			data:{id:id},
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success:function(data){
				$('input:hidden#it_day_id2'+itinerayact).remove();
				$('.it_act_2'+itinerayact).remove();
			},
			error:function(data){
				console.log(data);
			}
		});
		return;
	}
</script>
@endpush
