<div id="step-5">
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Features
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control features" id="features" name="features">{{ $record['features']['pf_title']  or  '' }}</textarea>
		</div>

	</div>
	@if (!is_null($record))
	<input type="hidden" name="id" value="{{ $record->pack_id }}">
	@endif
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ ($process=='edit')?'Update' :'ADD' }}
		</button>							
	</div>	
					
</div>
@push('footer')
<script src="{{ asset('admin-assets/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
		CKEDITOR.replace( 'features' );
</script>
@endpush