@extends('users.layouts.master')
@push('header')
<style type="text/css">
  .err
  {
    color: red !important;
    font-size: 14px !important;
  }
  .thumb{
        width: 200px !important;
        height: 200px !important;
    }
</style>
	<script type="text/javascript">
         jssor_1_slider_init = function() {
         
           var jssor_1_SlideshowTransitions = [
           {$Duration:800,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
           {$Duration:800,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
           ];
         
           var jssor_1_options = {
             $AutoPlay: 1,
             $SlideshowOptions: {
               $Class: $JssorSlideshowRunner$,
               $Transitions: jssor_1_SlideshowTransitions,
               $TransitionsOrder: 1
             },
             $ArrowNavigatorOptions: {
               $Class: $JssorArrowNavigator$
             },
             $ThumbnailNavigatorOptions: {
               $Class: $JssorThumbnailNavigator$,
               $SpacingX: 5,
               $SpacingY: 5
             }
           };
         
           var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
         
           /*#region responsive code begin*/
         
           var MAX_WIDTH = 980;
         
           function ScaleSlider() {
             var containerElement = jssor_1_slider.$Elmt.parentNode;
             var containerWidth = containerElement.clientWidth;
         
             if (containerWidth) {
         
               var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
         
               jssor_1_slider.$ScaleWidth(expectedWidth);
             }
             else {
               window.setTimeout(ScaleSlider, 30);
             }
           }
         
           ScaleSlider();
         
           $Jssor$.$AddEvent(window, "load", ScaleSlider);
           $Jssor$.$AddEvent(window, "resize", ScaleSlider);
           $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
           /*#endregion responsive code end*/
         };
      </script>
@endpush
@section('content')
	<section class="main-block " style="margin-top:7%;">
         <div class="container-fluid">
            <div class="row">
               <ol class="breadcrumb" style="background-color: transparent;;">
                  <li class="breadcrumb-item"><a href="{{ asset('/') }}">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{ route('packages',['id'=>Session::get('cat_id')]) }}"> {{ $packageDetails->category->cat_title }}</a></li>
                  @if(!is_null(Session::get('sc_id')))
                  <li class="breadcrumb-item"><a href="{{ URL::to('/getpackage-detail/'.Session::get('sc_id'))}}">{{ \App\SubCategory::find(Session::get('sc_id'))->sc_title }}</a></li>
                  @endif
                  <li class="breadcrumb-item active">{{ $packageDetails->pack_title }}</li>
               </ol>
          </div>
          <div class="row rowformsg" style="display: none">
              <div class="alert alert-success col-md-12 col-lg-12 col-sm-12 col-xs-12"><span class="alertmsg"></span></div>
          </div>
          <div class="row">
               <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                    @if(count($packageDetails['package_images'])>0)
                  <div class="row">
                     <!-- col-8 start jssor slider -->
                     <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:480px;overflow:hidden;visibility:hidden;">
                        <!-- Loading Screen -->
                        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                           <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
                        </div>
                        
                          @forelse($packageDetails['package_images'] as $pik=>$piv)
                          <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                           <div data-p="170.00">
                              <img data-u="image" src="{{ asset($piv->pi_img_path) }}" />
                              <img data-u="thumb" src="{{ asset($piv->pi_img_path) }}" />
                           </div>

                        </div>
                        <!-- Thumbnail Navigator -->
                        <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
                           <div data-u="slides">
                              <div data-u="prototype" class="p" style="width:190px;height:90px;">
                                 <div data-u="thumbnailtemplate" class="t"></div>
                                 <svg viewbox="0 0 16000 16000" class="cv">
                                    <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                    <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                    <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                                 </svg>
                              </div>
                           </div>
                        </div>

                        <!-- Arrow Navigator -->
                        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
                           <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                              <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                              <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                              <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                           </svg>
                        </div>
                        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
                           <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                              <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                              <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                              <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                           </svg>
                        </div>
                         @empty
                           @endforelse
                     </div>
                  </div>
                  @endif
                  <!--   slider end -->
                  <div class="packege-data">
                     <h4 class="w1"> Package consist of </h4>
                     <div class="pack">
                        <ul class="">
                          @foreach(explode("|", $packageDetails->pack_facilities) as $pack_flk=>$pack_flv)
                           <li  class="activity-details">
                              <i class="{{ \App\Facility::find($pack_flv)->facility_img_path }}" aria-hidden="true"></i>
                              <p>{{ \App\Facility::find($pack_flv)->facility_name }}</p>
                           </li>
                           @endforeach
                           
                        </ul>
                     </div>
                  </div>
                  @if(count($packageDetails['itinerary_days'])>0 || count($packageDetails['itinerary_acts'])>0)
                  <div class="panel-body mt20">
                     <h4 class="w1 ">Package Itinerary</h4>
                     <div class="accordion-data">
                        <div class="" id="accordion-tripping">
                           <div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">
                            @if(count($packageDetails['itinerary_days'])>0)
                              <div class="panel">
                                 <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                       <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                          <h6 class="">Days</h6>
                                       </a>
                                    </h4>
                                 </div>
                                 
                                <div id="collapseOne" class="panel-collapse collapse in show " role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                       <div class="tiny">
                                          <ul>
                                            @forelse($packageDetails['itinerary_days'] as $idk=>$idv)
                                             <li>
                                                {{ $idv->it_day_sub_title }}:-
                                              <p>{{ strip_tags($idv->it_day_desc) }}</p>
                                             </li>
                                             @empty
                                            @endforelse
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              @endif
                              <!-- end of panel -->
                               @if(count($packageDetails['itinerary_acts'])>0)
                              <div class="panel">
                                 <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                          <h6 class=""> Activity</h6>
                                       </a>
                                    </h4>
                                 </div>
                                 <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                       <div class="tiny">
                                          <ul>
                                             <li>
                                                
                                                 @forelse($packageDetails['itinerary_acts'] as $iak=>$iav)
                                                 <li>
                                                    {{ $iav->it_act_sub_title }}:-
                                                  <p>{{ strip_tags($iav->it_act_desc) }}</p>
                                                 </li>
                                                 @empty
                                                @endforelse
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endif
                              <!-- end of panel -->
                           </div>
                           <!-- end of #accordion -->
                        </div>
                     </div>
                  </div>
                  @endif
                  <!-- panel body end -->

                  <div class=" review-section">
                    <div class="row rowforreview" style="display: none;margin-top:10px;">
                          <div class="alert alert-success col-md-12 col-lg-12 col-sm-12 col-xs-12"><span class="alertforreviewmsg"></span></div>
                     </div>
                     <div class="row mt">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <h3>Top Reviews</h3>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                           <button class="btn btn-expand " data-toggle="modal" data-target="#exampleModal" style="font-weight: bold;
                              color: #000000;
                              background: #e7e7e7; display: block;margin: 0 auto;">Submit Review</button>
                        </div>
                     </div>
                     <hr>
                     @if(count($reviewsList)>0)
                     <section class="reviews_list">
                       @include('users.include.reviews_list')
                     </section>
                     {{-- <button class="btn btn-expand" style="font-weight: bold;
                        color: #000000;
                        background: #e7e7e7;">Read more Reviews
                     </button>  --}}
                     @else
                        <div class="col-md-12 col-sm-12 col-xs-12" style="float: right;">
                          <p>No any reviews</p>
                        </div>
                     @endif

                            
                  </div>
               </div>
               <!-- col-8 end jssor slider -->
               <div class="col-md-4 col-xs-12" >
                  <div class="container">
                     <h3 class=" " style="color: #a74500;font-size: 23px;">{{ $packageDetails->pack_title }}</h3>
                     <hr>
                     <div class="clearfix"></div>
                     <div class="row">
                        <div class="btn-group ">
                           <a type="button" class="btn btn-sm btn-outline-secondary b-r">{{ $packageDetails->pack_days }} Days
                           </a>
                           <a type="button" class="btn btn-sm btn-outline-secondary">{{ $packageDetails->pack_nights }} Nights</a>
                        </div>
                     </div>
                     <div class="row mt">
                        @if($packageDetails->pack_type=='event')
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mt">
                           <label class="label12">Select Date:</label>
                           <select class=" form-control mnth mt datepicker" id="edp_date" name="edp_date" onchange="getpriceBasedOnDate()">
                            <option>Select Date:</option>
                           @forelse(\App\EventDateprice::where('edp_pack_id',$packageDetails->pack_id)->get() as $edpKey=>$edpValue)
                            <option value="{{ $edpValue->edp_id }}" {{ ($edpKey==0)?'selected':'' }}>{{ $edpValue->edp_date }}</option>
                           @empty
                           @endforelse
                           </select>
                           {{-- <input type="text" class=" form-control mnth mt datepicker" placeholder="Select Date" id="datepicker1"> --}}
                        </div>
                        @endif
                        @if($packageDetails->pack_type=='package')
                        @php
                            $month=array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December')
                        @endphp
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mt">
                           <label class="label12">Select Month:</label>
                           <select name="month" class="form-control mnth " placeholder="Select Month" id="month" style="border:1px solid #ddd; margin-top: 10px;">
                              <option value="-1">Select Month</option>
                              @forelse($month as $mk=>$mv)
                              <option value="{{ $mk }}">{{ $mv }}</option>
                              @empty
                              @endforelse
                           </select>
                        </div>
                        @endif
                     </div>

                     <div class="row">  
                       @if(count($packageDetails['pack_features'])>0)
                        <h3 class="my-4 "> Main Features</h3>
                        <div class="tiny">
                           
                              {{-- <li>Accommodation &amp; transportation on the Solmar V</li>
                              <li>Snacks, soft drinks and beer</li>
                              <li>Cage diving while on the Solmar V</li> --}}
                              @forelse($packageDetails['pack_features'] as $pfk=>$pfv)
                             {!!html_entity_decode(strip_tags($pfv->pf_title,"<li>"))!!}
                              
                              @empty
                              @endforelse
                           
                        </div>
                        @endif
                        <div class="wild-price-info">
                           <p class="final"><span class="dis  p-t b-r">₹ {{ $packageDetails->pack_discount }}</span>₹ <span class="changeprice">{{ $packageDetails->pack_price }}</span> /  <span class="per_person_new">per adult</span></p>
                        </div>
                        <div class="btn-group">
                            @if($packageDetails->pack_type=='package')
                            <center><div class=" col-md-6 col-lg-6 col-sm-6 col-xs-12 mt">
                                <button type="button" data-toggle="modal" data-target="#exampleModal2"  class="btn  b13">Send Enquiry</button>
                            </div></center>
                            @else
                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mt">
                            <form method="post" name="booknow_form" id="booknow_form" action="{{ route('pay_view')}}">
                            
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="price" value="" class="price">
                              <input type="hidden" name="id" value="{{ $packageDetails->pack_id }}">
                              <button type="submit" data-toggle="modal" data-target="#booking-modal"  class="btn  b12 ">Book Now</button>
                            
                          </form>
                          </div>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="row">
                 
                     </div>
                     @if(count($related_packages)>0)
                     <div class="row mt" style="margin-top: 20px;">
                        <h4 class="text-center T1">Related Packages</h4>
                        <hr>
                        
                     </div>
                     @foreach($related_packages as $rpk=>$rpv)
                     <div class="row " style="margin-top: 20px;">
                        <div class="">
                           <div class="cuadro_intro_hover " style="background-color:#fff;">
                              <p class="p1">
                                @php 
                                $image=\App\PackageImage::where('pi_pack_id',$rpv->pack_id)->first();
                                @endphp
                                 <img src="{{ asset($image['pi_img_path']) }}" class="img-responsive" alt="">
                              </p>
                              <div class="caption">
                                 <div class="blur"></div>
                                 <div class="caption-text">
                                    <a class=" btn btn-default button-book" href="{{ route('package-view',['id'=>$rpv->pack_id]) }}"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
                                    <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
                                       <span class="span-disc">Jumbo Discount Price From</span>&#8377;{{ $rpv->pack_price }}
                                    </h3>
                                    <p>{{ $rpv->pack_title }}</p>
                                    <p>{{ $rpv->pack_days }} Days/{{ $rpv->pack_nights }} nights:<span class="span-disc">ASHG</span></p>
                                 </div>
                              </div>
                           </div>
                           <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
                              {{ \App\City::find($rpv->pack_city)->city_name }}
                           </p>
                        </div>
                     </div>
                     @endforeach
                     
                     @endif
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
         <!-- send enquiry-->
         <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog md-dialog" role="document" style=" max-width: 555px !important; margin: 30px auto;">
               <div class="modal-content" >
                  <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Get in touch with our Travel Expert</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                  </div>
                  <div class="modal-body">
                     <!--  modal body start -->
                     <!--  contact sectin -->
                     <section id="contact">
                        <div class="contact-section">
                           <div class="container">
                              <form method="post" id="form_enquiry" name="form_enquiry">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="eq_pack_id" value="{{ $packageDetails->pack_id }}">
                                 <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 form-line ">
                                    <div class="form-group">
                                       <input type="text" class="form-control1" id="name" placeholder=" Enter Name" name="eq_user">
                                       <p class="err_name err"><span class="err_name_txt"></span></p>
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control1" id="email" placeholder=" Enter E-mail" name="eq_email">
                                        <p class="err_email err"><span class="err_email_txt"></span></p>

                                    </div>
                                    <div class="form-group">
                                       <input type="tel" class="form-control1" id="mobile" placeholder=" Enter 10-digit mobile no." name="eq_mobile">
                                       <p class="err_mobile err"><span class="err_mobile_txt"></span></p>
                                    </div>
                                    <div class="form-group">
                                       <textarea class="form-control1" placeholder="Query" name="eq_query" id="query"></textarea>
                                       <p class="err_query err"><span class="err_query_txt"></span></p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </section>
                     <!--  contact sectin -->
                  </div>
                  <!--  modal body end -->
                  <div class="modal-footer buttonshow">
                     <!--  modal footer start -->
                     <button type="button" class="btn btn-primary sendenquirnbtn" style="background-color: #C05900;color: #fff;" onclick="sendEnquiry()"><i class="fa fa-paper-plane" aria-hidden="true" ></i>  Send  Message</button>
                  </div>
                  <!--  modal footer end -->
               </div>
            </div>
         </div>
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog lg-dialog" role="document" style="max-width: 741px;
               margin: 30px auto;
               ">
               <div class="modal-content" >
                  <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Submit Review</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                  </div>
                  <div class="modal-body">
                     <section>
                        <div class="container">
                          <form method="post" name="review_form" id="review_form" enctype="multipart/form-data">
                            <input type="hidden" name="rv_pack_id" value="{{ $packageDetails->pack_id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <div class="row">
                              <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                                 <div class="row">

                                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                       <div class="form-group">
                                          <input type="text" class="form-control1" id="rv_u_name" placeholder=" Enter Name" name="rv_u_name">
                                          <p class="err_rv_name err"><span class="err_rvname_txt"></span></p>
                                       </div>
                                      </div>
                                      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                       {{-- <div class="form-group">
                                          <input type="text" class="form-control1" id="exampleInputEmail" placeholder=" Enter Date">
                                       </div> --}}
                                       <div class="form-group">
                                        
                                          <input type="tel" class="form-control1" id="rv_u_phone" placeholder=" Enter 10-digit mobile no." name="rv_u_phone">
                                          <p class="err_rv_phone err"><span class="err_rvphone_txt"></span></p>
                                       </div>
                                       
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <input type="email" class="form-control1" id="rv_u_email" placeholder=" Enter Email id" name="rv_u_email">
                                          <p class="err_rv_email err"><span class="err_rvemail_txt"></span></p>
                                       </div>
                                       {{-- <div class="form-group">
                                          <input type="text" class="form-control1" id="exampleInputEmail" placeholder=" Select package">
                                       </div> --}}
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                      <div class="form-group">
                                         <label for="description"></label>
                                         <textarea class="form-control1" id="rv_content" placeholder=" Please write at least 100 characters about your experience at this destination." name="rv_content"></textarea>
                                         <p class="err_rv_content err"><span class="err_rvcontent_txt"></span></p>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-lg-4 col-sm-4  col-xs-12">
                                 <div class="" style="float: right;">
                                      <div id="thumb-output"></div>
                                   <div id="thumb-default"><img src="{{ asset('images/jessy.jpeg') }}"  class="img4 img-fluid thumb"></div>
                                    
                                    <h3 class="dark2 " style="text-align: center;">
                                       <input name="pro" class="upload" id="file-2" type="file" data-multiple-caption="{count} files selected" multiple="" accept="image/png, image/jpeg, image/jpg" style="display: none;" name="rv_u_photo">
                                       <label for="file-2" class="fileUpload upload" id="labelPic">
                                       <i class="fa fa-edit" aria-hidden="true"></i> Add Photo</label>
                                    </h3>
                                 </div>
                              </div>
                           </div>
                          </form>
                     </section>
                     </div>
                     <!--  modal body end -->
                     <div class="modal-footer buttonreviewshow">
                        <!--  modal footer start -->
                        <button type="button" class="btn btn-primary sendreviewbtn" style="background-color: #C05900;color: #fff;" onclick="postReview()"><i class="fa fa-paper-plane" aria-hidden="true" ></i>  Send  Review</button>
                     </div>
                     <!--  modal footer end -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      @push('footer')
      <script src="{{ asset('js/jssor.slider-27.1.0.min.js') }}" type="text/javascript"></script>
      <script type="text/javascript">jssor_1_slider_init();</script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.3.7/jquery.jscroll.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function(){
          @if($packageDetails->pack_type=='event')
            var id=$('#edp_date').val();
            $.ajax({
                url: "{{ route('price') }}",
                type:'POST',
                data:{
                    id:id,
                    "_token": "{{ csrf_token() }}"},
                    success: function(data){
                        $('.changeprice').html(data);
                        $('.price').val(data);
                    },
                    error: function(data){
                        console.log(data);
                    }

            });
            @endif
        });
         function getpriceBasedOnDate()
         {
            var id=$('#edp_date').val();
            $.ajax({
                url: "{{ route('price') }}",
                type:'POST',
                data:{
                    id:id,
                    "_token": "{{ csrf_token() }}"},
                    success: function(data){
                        $('.changeprice').html(data);
                        $('.price').val(data);
                    },
                    error: function(data){
                        console.log(data);
                    }

            });
         }
         function sendEnquiry()
         {
              var namestatus=emailstatus=mobilestatus=querystatus=0;
              namestatus=emptyValidation('name','err_name','err_name_txt',"Please enter your name");
              emailstatus=emptyValidation('email','err_email','err_email_txt',"Please enter your email");
              mobilestatus=emptyValidation('mobile','err_mobile','err_mobile_txt',"Please enter your mobile");
              querystatus=emptyValidation('query','err_query','err_query_txt',"Please enter your query");
              if(namestatus==0 || emailstatus==0 || mobilestatus==0 || querystatus==0)
              {
                return false
              }
              else
              {
                $('.sendenquirnbtn').hide();
                $('.buttonshow').append('<button class="buttonload btn btn-primary" style="float:right;background-color: #C05900;color: #fff;"><i class="fa fa-spinner fa-spin"></i>Processing</button>');
                var fd = new FormData($('#form_enquiry')[0]);
                var url="{{ route('enquiry') }}";
                $.ajax({
                  type:'POST',
                  url:url,
                  data:fd,
                  processData : false,
                  contentType : false,
                  success:function(data){
                  if(data=='success')
                  {
                    $('#form_enquiry')[0].reset();
                      $("#exampleModal2").modal("hide");
                      $('.rowformsg').show();
                      $('.alertmsg').html("Thanks you for sending enquiry,we quickly contacting us..");
                       setTimeout(function() {
                        $(".rowformsg").hide()
                    }, 5000);
                  }
                },
                error:function(data){
                  console.log(data);
                }
              });
            }
              
         }
         $('#email').keyup(function() {
          if ($('#email').val() != '') {
            var email=$('#email').val();
            if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
            {
               $('.err_email').show();
                    $('.err_email_txt').html('Please enter valid email address');
                    return false; 
            }
            else
            {
                $('.err_email').hide();
            }
        } });
         $("#mobile").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
               // Allow: Ctrl+A, Command+A
              (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
               // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) {
                   // let it happen, don't do anything
                   return;
          }
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });
        $('#name').bind('keypress', function (event) {
          var regex = new RegExp("^[a-zA-Z, ,\t,\b]+$");
          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
          if (!regex.test(key)) {
             event.preventDefault();
             return false;
          }
      });
        function emptyValidation(id,errclass,errmsg_class,errmsg)
        {
              var val=$('#'+id).val();
              if(val=='')
              {
                  $('.'+errclass).show();
                  $('.'+errmsg_class).html(errmsg);
                  return 0;
              }
              else{
                $('.'+errmsg_class).html('');
                $('.'+errclass).hide();
                return 1;
              }
        }

        function postReview()
        {
          var namestatus=emailstatus=mobilestatus=querystatus=0;
              namestatus=emptyValidation('rv_u_name','err_rv_name','err_rvname_txt',"Please enter your name");
              emailstatus=emptyValidation('rv_u_email','err_rv_email','err_rvemail_txt',"Please enter your email");
              mobilestatus=emptyValidation('rv_u_phone','err_rv_phone','err_rvphone_txt',"Please enter your mobile");
              querystatus=emptyValidation('rv_content','err_rv_content','err_rvcontent_txt',"Please enter your review");
              if(namestatus==0 || emailstatus==0 || mobilestatus==0 || querystatus==0)
              {
                return false
              }
              else
              {
                $('.sendreviewbtn').hide();
                $('.buttonreviewshow').append('<button class="buttonload btn btn-primary" style="float:right;background-color: #C05900;color: #fff;"" ><i class="fa fa-spinner fa-spin"></i>Processing</button>');
                var fd = new FormData($('#review_form')[0]);
                var url="{{ route('reviews') }}";
                $.ajax({
                  type:'POST',
                  url:url,
                  data:fd,
                  processData : false,
                  contentType : false,
                  success:function(data){
                      console.log(data);
                  if(data=='success')
                  {
                    $('#review_form')[0].reset();
                      $("#exampleModal").modal("hide");
                      location.reload();
                      $('.rowforreview').show();
                      $('.alertforreviewmsg').html("Thanks you for your review.");
                       setTimeout(function() {
                        $(".rowforreview").hide()
                    }, 5000);
                  }
                },
                error:function(data){
                  console.log(data);
                }
              });
            }
        }
        $('#file-2').on('change', function(){ //on file input change
            if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
            {
                $('#thumb-output').html(''); //clear html of output element
                var data = $(this)[0].files; //this file data
               
                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                            $('#thumb-default').hide(); 
                            $('#thumb-output').append(img); //append image to output element
                        };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });
               
            }else{
              $('#file-2').html('');
                 //if File API is absent
            }
        });
        $('.jscroll1_pagi').hide();
     $(function() {
        $('.jscroll1').jscroll({
            debug: true,
            autoTrigger : true,
            nextSelector : '.jscroll1_pagi .pagination li.active + li a',
            contentSelector : 'div.jscroll1',
            callback: function(){
               
                $('.jscroll1_pagi .pagination:visible:first').hide();
            },
            loadingHtml : '<div class ="loading-forum">Loading...</div>'
        });
    });
      </script>
      @endpush
@endsection
