<div class="jscroll1">
   @forelse($reviewsList as $reviewKey=>$reviewVal)
<div class="one1">
   <div class="col-md-4 col-md-4 col-xs-4 review-part-left" style="float: left;">
      <div class="row">
         <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            @if($reviewVal->rv_u_photo=='' || !(file_exists($reviewVal->rv_u_photo)))
                  @php $imgpath='images/mm.png' @endphp
            @else
                  @php $imgpath=$reviewVal->rv_u_photo @endphp
            @endif
            <img src="{{ asset($imgpath) }}">
         </div>
         <div class="col-md-6 col-sm-6 col-xs-12 re  b-r">
            <p>{{ $reviewVal->addedDate }}</p>
            <span>{{ $reviewVal->rv_u_name }}</span><br>
         </div>
      </div>
   </div>
   <div class="col-md-8 col-sm-8 col-xs-8 review-part-right" style="float: right;">
      <div class="">
         
         <div class="col-md-12 col-sm-12 col-xs-12">
            <p>{{ $reviewVal->rv_content }}
            </p>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
</div>
@empty
@endforelse
<div class="jscroll1_pagi"> {{ $reviewsList->links() }}</div>
</div>




