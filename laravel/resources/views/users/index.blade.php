@extends('users.layouts.master')
@push('header')
<style type="text/css">
   a .btn1 {
       color: #FF4500;
       font-weight: bold;
       letter-spacing: 1px;
       font-size: 18px;
       font-style: italic;
   }
   .select2
   {
      width: 100% !important;

    position: relative;
    
    
    border-top: none;
    border-right: none;
    border-bottom: none;
    /*border-left: 1px solid #ccc;*/
      
   }
   .selection
   {
      width: 34% !important;
      /*padding: 15px !important;*/
   }
</style>
  <link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@endpush
@section('content')
	
	<section class="slider d-flex align-items-center main1">
      <div class="container">
         <div class="row d-flex justify-content-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="slider-title_box">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-content_wrap ">
                           <h2 class="head">
                              Say <b style="color: #ffb000;">yes</b> to new<b style="color: #ffb000;">adventures</b>
                           </h2>
                           <p class="wild-index ">People don’t take trips, Trips take People.</p>
                        </div>
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center">
                     <div class="col-md-10 col-sm-10 col-lg-10 col-xs-12">
                        <form class="form-wrap mt-4" id="searchBar"  method="post" action="{{ route('search-result') }}">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <div class="btn-group searchbar" id="searchBox" role="group" aria-label="Basic example">
                              <select   class="btn-group1 searchtext" style="width:100%;"  name="searchtext"></select>
                              <a onclick="packageList()"><i class="fa fa-search search1"></i></a>
                              {{-- <input type="text" placeholder="Search for Tours, Activities,and Experiences...."  class="btn-group1" style="width:100%;"/>
                              <a onclick="packageList()"><i class="fa fa-search search1"></i></a> --}}
                           </div>
                        </form>
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center">
                     <div class="col-md-10 col-sm-10 col-lg-10 col-xs-12 adv-search" style="display: none">
                        <form class="form-wrap mt-4" id="searchData_Form" method="post" action="{{ route('search-result') }}">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <input type="hidden" name="searchadvancetext" id="searchadvancetext">
                           <div class="btn-group adv-search" role="group" aria-label="Basic example">
                             {{--  <input type="text" class="btn-group2" placeholder="Select Month" id="datepicker"/> --}}
                              @php
                                   $month=array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December')
                              @endphp
                             <select class="btn-group2" name="month">
                                <option>Select Month</option>
                                  @forelse($month as $mk=>$mv)
                                    <option value="{{ $mk }}">{{ $mv }}</option>
                                    @empty
                                 @endforelse
                             </select>
                              <input type="text" placeholder="No Of days" class="btn-group2" name="days"/>
                              <select  placeholder="Category" class="category" name="cat_id"></select>
                           </div>
                        </form>
                     </div>
                  </div>
                  <div class="scrollbar style-4   no-padding hidden" id="searchDrop" style="display: none;">
                     <div class="force-overflow">
                     </div>
                  </div>
                  <div class="slider-link">
                     <div class="adv wild-index" style="padding-right: 95px;">
                        <p>
                           <i class="fa fa-plus" aria-hidden="true"></i> Advanced Search
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--// SLIDER -->
   <!--//END HEADER -->
   <!--Top Trending -->
   <section class="main-block main2">
      <div class="container my-3">
         <div class="row">
            <div class="styled-heading">
               <h2 class="head2">Top Trending</h2>
            </div>
            <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
               <div class="carousel-inner w-100" role="listbox">
                  @php $countfortop=0 @endphp
                  @forelse(\App\Package::where('pack_top_trand',1)->orderBy('pack_id','desc')->where('pack_delete',0)->get() as $pk=>$pv)
                  @if($pk==0 || $pk==4)
                     <div class="carousel-item row no-gutters {{ ($pk==0)? 'active' :'' }}">
                  @endif
                     <a href="{{ URL::to('/get-package-detail/'.$pv->pack_id) }}">
                     <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12  mt float-left"   style="padding-right: 5px;">

                        <div class="section-box-eleven">
                            <figure>
                              <h5>{{ str_limit($pv->pack_title,15) }}</h5>
                              <p></p>
                           </figure>
                           @php
                              $img=$pv->packageImages()->first();
                              $imagepath=(!is_null($img))?$img->pi_img_path:'images/no-image.png'
                           @endphp
                           <img src="{{ asset($imagepath) }}" class="img-responsive"/>
                        </div>

                     </div>
                  </a>
                  @if($pk==3 || $pk==7)
                  </div>
                  @endif
                  @php $countfortop++ @endphp
                  @empty

                  @endforelse
                  </div>
               @if(($countfortop>4))
                  <a class="carousel-control-prev caro-next" href="#recipeCarousel" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next caro-next2" href="#recipeCarousel" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                  </a>
               @endif
         </div>
      </div>
   </div>
</section>
<!--Top Trending -->
<!--Travel Experiences -->
{{-- <section class="main-block main3">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="styled-heading">
               <h2 class="head2" style="font-size: 21px; text-align: left">Travel Experiences Across the Globe </h2>
            </div>
         </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12  no-padding">
         <div class=" find-img-align no-padding">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q2.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a3">NORTH EAST INDIA</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
         <div class="">
            <div class="find-place-img_wrap no-padding">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q3.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a3">CAMPING</h3>
                     </div>
                  </figure>
               </div>
            </div>

         </div>
      </div>
      <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12  no-padding">
         <div class=" find-img-align no-padding">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q4.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a2">BIRD WATCHING</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
         <div class="">
            <div class="find-place-img_wrap no-padding">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q5.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a1">THE HIMALAYA</h3>
                     </div>
                  </figure>
               </div>
            </div>

         </div>
      </div>
      <div class="col-md-2 col-sm-2 col-lg-2 col-xs-12  no-padding">
         <div class=" find-img-align no-padding">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q6.jpg" class="img-fluid" alt="img13" />
                     <div>
                        <h3 class="a3">WATER SPORTS</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
         <div class=" no-padding">
            <div class="find-place-img_wrap ">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q7.jpg" class="img-fluid" alt="img13" />
                     <div>
                        <h3 class="a3">WATERFALL</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 no-padding">
         <div class=" find-img-align no-padding">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q8.jpg" class="img-fluid" alt="img13" />
                     <div>
                        <h3 class="a1">SAFARI</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
         <div class="">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q9.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a1">COASTLE TOURS</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12  no-padding">
         <div class=" find-img-align no-padding">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q10.jpg" class="img-fluid" alt="img13" style=""/>
                     <div>
                        <h3 class="a2">FORT</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 no-padding">
         <div class=" find-img-align ">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q11.jpg" class="img-fluid" alt="img13" style=""/>
                     <div>
                        <h3 class="a1">THE HIMALAYA</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12  no-padding">
         <div class=" find-img-align">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q1.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a2">HIKING</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12  no-padding">
         <div class=" find-img-align">
            <div class="find-place-img_wrap">
               <div class="grid">
                  <figure class="effect-ruby">
                     <img src="images/q3.jpg" class="img-fluid" alt="img13" style="" />
                     <div>
                        <h3 class="a1">CAMPING</h3>
                     </div>
                  </figure>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</section> --}}
<section class="main-block main3">
   <div class="container custom-collapse">
      <div class="row justify-content-center">
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="styled-heading">
               <h2 class="head2" style="font-size: 21px; text-align: left">Travel Experiences Across the Globe </h2>
            </div>
         </div>
      </div>
      <div class="row justify-content-center ">
         @forelse(\App\IndexCategory::take(12)->where('ic_delete',0)->get() as $ick=>$icv)
         @if($ick==0)
            <ul class="inter-demo">
         @elseif($ick==4)
            <ul class="inter-demo2">
         @elseif($ick==8)
            <ul class="inter-demo3">
         @endif
            <li>
               <a href="{{ $icv->ic_link }}">
              <div class=" find-img-align no-padding">
               <div class="find-place-img_wrap">
                  <div class="grid">
                     <figure class="effect-ruby">
                        <img src="{{ asset($icv->ic_image) }}" class="img-fluid" alt="img13" style="" />
                        <div>
                           <h3 class="a3">{{ $icv->ic_title }}</h3>
                        </div>
                     </figure>
                  </div>
               </div>
            </div>
         </a>
         </li>
         @if($ick==3 || $ick==7 || $ick==11)
            </ul>
         @endif
         @empty
         @endforelse
        
           
   


</div>
</div>
</section>
<!-- Travel Experiences -->
<!-- Wildtrippin Services-->
<section class="main-block Wildtrippin-Services main4">
   <div class="container text-center my-3">
      <div class="row">
         <div class="styled-heading">
            <h2 class="head2"> Wildtrippin Services</h2>
         </div>
         <div id="carousel123" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner w-100" role="listbox">
               @php $countforservice=0 @endphp
               @php $temp=0 @endphp
                  @forelse(\App\Service::where('s_delete',0)->get() as $sk=>$sv)
                  @php $countforservice=$sk @endphp
                  
                  
                     @if(($temp)==($countforservice))
                     @php $temp=$temp+4 @endphp
                     <div class="carousel-item row no-gutters {{ ($sk==0)?'active':'' }}">
                     @endif
                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 mt main4 float-left">
                           <div class="feature-col">
                              <div class="card card-block text-center">
                                 <div class="hover09">
                                    <figure>
                                       <img src="{{ asset($sv->s_img_path) }}" class="img-fluid" width="100%">
                                    </figure>
                                 </div>
                                 <div class="news-content">
                                    <h6>
                                       <a href="{{ route('services',['id' => $sv->s_id]) }}">{{ $sv->s_title }}</a>
                                    </h6>
                                    <p>
                                       {{ $sv->s_desc }}
                                    </p>
                                    <div class="clearfix"></div>
                                    <!-- clearfix -->
                                    <a href="{{ route('services',['id' => $sv->s_id]) }}"><button class="btn1">Explore</button></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @if(($temp-1)==($countforservice))
                     </div>
                     <a class="carousel-control-prev caro-next" href="#carousel123" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true" style="height: 46px;width: 42px;"></span>
                     <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next caro-next2" href="#carousel123" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true" style="height: 46px;width: 42px;"></span>
                     <span class="sr-only">Next</span>
                  </a>
                        @endif
                        
                  @empty
                  @endforelse
                  
                 
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Wildtrippin Services -->
<!-- gallary -->
{{-- <section class="main-block main5 ">
   <div class="container  respo2">
      <div class="styled-heading">
         <h2 class="head2"> Wildtrippin Global Tours</h2>
      </div>
      <div class="row">
         <div class="col-md-5 col-sm-5 col-lg-5 col-xs-12 caption-style-4 hover11   no-padding">
            <li>
               <img src="images/a1.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>MALDIVES</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 caption-style-4 hover11   no-padding">
            <li>
               <img src="images/a2.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>DUBAI</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a3.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>VIETNAM</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
      </div>
      <div class="row">
         <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a4.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>AFRICA</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-2 col-sm-2 col-lg-2 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a5.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>NEPAL</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-3 col-lg-3  col-sm-3 col-xs-12  caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a6.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>EUROPE</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a7.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>EGYPT</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a8.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>BALI</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
      </div>
      <div class="row">
         <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a9.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>INDIA</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-2 col-sm-2 col-lg-2 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a10.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>CHINA</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-3 col-lg-3  col-sm-3 col-xs-12  caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a11.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>SRI LANKA</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a4.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>COMBODIA</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div>
        <!--  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 caption-style-4 hover11    no-padding">
            <li>
               <img src="images/a5.jpg" class="img-fluid" width="100%">
               <div class="caption">
                  <div class="blur"></div>
                  <div class="caption-text">
                     <h1>DUBAI</h1>
                     <p>Always Awesome</p>
                  </div>
               </div>
            </li>
         </div> -->
      </div>
   </div>
</section> --}}
<section class="main-block main5 ">
   <div class="container  respo2 ">
      <div class="styled-heading">
         <h2 class="head2"> Wildtrippin Global Tours</h2>
      </div>
      @php $count=1
      @endphp
         @forelse(\App\SubCategory::take(7)->where('sc_delete',0)->where('sc_cat_id',3)->get() as $itk=>$itv)
         @if($count==1 || $count==4)
            <div class="row">
             @if($count==4)  
             <ul class="demo-data1">
               @else
               <ul class="demo-data">
               @endif
                 
         @endif
               <li class="caption-style-4">
                   <div class="hover11 no-padding">
                     <img src="{{ asset($itv->sc_image) }}" class="img-fluid" >
                     <div class="caption">
                        <div class="blur"></div>
                        <div class="caption-text">
                           <h1>{{ $itv->sc_title }}</h1>
                           <p>{{ $count }}</p>
                        </div>
                     </div>
                  </div>
               </li>
              @if($count==3 || $count==7)
            </ul>
         </div>
         @endif
         @php $count++ @endphp
         @empty
         @endforelse
      
</div>
</section>

@endsection
@push('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
         $('.category').select2({

        placeholder: 'Select an item',

        ajax: {

          url: '{{ route('categorylist') }}',

          dataType: 'json',

          delay: 250,

          processResults: function (data) {
               console.log(data);
            return {

              results: data

            };

          },

          cache: true

        }

      });

         function packageList()
         {
            if($('.adv-search:visible').length == 0)
            {
                  $('#searchBar').submit();
            }
            else
            {
               $('#searchadvancetext').val($('.searchtext').val())
               $('#searchData_Form').submit();
            }
            
       }
        $('.searchtext').select2({

      //  placeholder: 'Search for Tours, Activities,and Experiences....',

        ajax: {

          url: '{{ route('packagetitlelist') }}',

          dataType: 'json',

          delay: 250,

          processResults: function (data) {
               console.log(data);
            return {

              results: data

            };

          },

          cache: true

        }

      });

</script>
@endpush