<footer class="main-block dark-bg main6">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
            <div class="row">
               <div class="col-md-3 col-lg-3  col-sm-3 col-xs-12">
                  <h2 class="head3"> walk-around</h2>
                  <ul class="walk-around-data">
                     <li><a href="{{ URL::to('/') }}">Home</a></li>
                     <li><a href="about-us">About Us</a></li>
                     <li><a href="contact-us">Contact us</a></li>
                  </ul>
               </div>
               <div class="col-md-3 col-lg-3  col-sm-3 col-xs-12">
                  <h2 class="head3"> Services</h2>
                  <ul class="walk-around-data">
                    @forelse(\App\Service::where('s_delete',0)->get() as $sk=>$sv)
                      <li><a href="{{ route('services',['id' => $sv->s_id]) }}">{{ $sv->s_title }}</a></li>
                    @empty
                    @endforelse
                  </ul>
               </div>
               <div class="col-md-3 col-lg-3  col-sm-3 col-xs-12">
                  <h2 class="head3"> International Tours</h2>
                  <ul class="walk-around-data">
                     @forelse(\App\SubCategory::where('sc_delete',0)->where('sc_cat_id',2)->get() as $sck=>$scv)
                      <li><a href="{{ URL::to('/getpackage-detail/'.$scv->sc_id) }}">{{ $scv->sc_title }}</a></li>
                      @empty
                    @endforelse
                  </ul>
               </div>
               <div class="col-md-3 col-lg-3  col-sm-3 col-xs-12 address-data">
                  <h2 class="head3">  address</h2>
                  <div>
                     <p>#103, Pioneer Tower</p>
                     <p>vdffvbfdggf, sdfgdsfgf</p>
                     <p>Email   : info@Wildtrippin.in</p>
                     <p>Phone : 21234231432, 423433 9999</p>
                     <br>        
                  </div>
               </div>
            </div>
            <div class="">
               <div class="design-by">
                  <span class="data12">
                     <p>Copyright © 2018 Wildtrippin.in</p>
                  </span>
                  <span class="design"><a href="http://Sungare.com/" target="_blank">Designed By : Sungare.com</a></span>
                  <div class="clear"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/pikaday.js') }}"></script>
<script type="text/javascript">
   (function(){

                        // https://getbootstrap.com/javascript/#carousel
                        $('#carousel123').carousel({ interval: 2000 });
                        $('#carouselABC').carousel({ interval: 3600 });
                     }());

   (function(){
     $('.carousel-showmanymoveone .item').each(function(){
       var itemToClone = $(this);

       for (var i=1;i<4;i++) {
         itemToClone = itemToClone.next();

                            // wrap around if at end of item collection
                            if (!itemToClone.length) {
                              itemToClone = $(this).siblings(':first');
                           }

                            // grab item, clone, add marker class, add to collection
                            itemToClone.children(':first-child').clone()
                            .addClass("cloneditem-"+(i))
                            .appendTo($(this));
                         }
                      });
  }());
</script>
<script>
   var flip = 0;
   $( ".adv" ).click(function() {
     $( ".adv-search" ).toggle( flip++ % 2 === 0 );
  });
</script>
<script>
   var picker = new Pikaday(
   {
     field: document.getElementById('datepicker'),
     firstDay: 1,
     minDate: new Date(),
     maxDate: new Date(2020, 12, 31),
     yearRange: [2000,2020]
  });

</script>
<script type="text/javascript">
   $(window).scroll(function()
   {


    if ($(window).scrollTop() > 100) {

      $('.fixed').addClass('is-sticky');

   } else {

      $('.fixed').removeClass('is-sticky');

   };
});

</script>
@stack('footer')
</body>
</html>


