<html>
<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="author" content="sungare technology">
   <meta name="description" content="#">
   <meta name="keywords" content="#">
   <!-- Page Title -->
   <title>Wildtrippin</title>
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
   <!-- Simple line Icon -->
   <link rel="stylesheet" href="{{ asset('css/simple-line-icons.css') }}">
   <!-- Themify Icon -->
   <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
   <!-- Hover Effects -->
   <link rel="stylesheet" href="{{ asset('css/set1.css') }}">
   <link rel="stylesheet" href="{{ asset('css/pikaday.css') }}">
   <!-- Main CSS -->
   <link rel="stylesheet" href="{{ asset('css/style.css') }}">
   <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
   <style type="text/css">

      .carousel-control-next, .carousel-control-prev {
        position: absolute;
        top: 0;
        bottom: 0;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 5%;
        color: #fff;
        text-align: center;
        opacity: .8 !important;
     }
    </style>
  @stack('header')
</head>
<body>
   <!-- HEADER -->
   <div class="nav-menu">
      <div class="bg transition">
         <div class="container-fluid fixed">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                  <nav class="navbar navbar-expand-lg navbar-light">
                     <a href="{{ asset('/') }}" class="navbar-brand " style="margin-top: -8px;"><img src="{{ asset('images/logo.png') }}" class="img-fluid"></a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-menu"></span>
                     </button>
                     <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            @foreach(\App\Category::where('cat_delete',0)->get() as $catk=>$catv)
                                <li class="nav-item dropdown">
                                  <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                     {{ $catv->cat_title }}
                                     <span class="icon-arrow-down"></span>
                                  </a>
                                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                   @foreach(\App\SubCategory::where('sc_delete',0)->where('sc_cat_id',$catv->cat_id)->get() as $scatk=>$scatv)
                                  
                                     <a class="dropdown-item" href="{{ URL::to('/getpackage-detail/'.$scatv->sc_id) }}">{{ $scatv->sc_title }}</a>
                                     
                                    @endforeach
                                     </div>    
                               </li>
                            @endforeach
                           {{-- <li class="nav-item dropdown">
                              <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 family tour
                                 <span class="icon-arrow-down"></span>
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                 <a class="dropdown-item" href="#">  family tour</a>
                                 <a class="dropdown-item" href="#">  family tour</a>
                                 <a class="dropdown-item" href="#">  family tour</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 family tour
                                 <span class="icon-arrow-down"></span>
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                 <a class="dropdown-item" href="#">  family tour</a>
                                 <a class="dropdown-item" href="#">  family tour</a>
                                 <a class="dropdown-item" href="#">  family tour</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 family tour
                                 <span class="icon-arrow-down"></span>
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                 <a class="dropdown-item" href="#">  family tour</a>
                                 <a class="dropdown-item" href="#">  family tour</a>
                                 <a class="dropdown-item" href="#">  family tour</a>
                              </div>
                           </li>
                           <li class="nav-item active">
                              <a class="nav-link" href="#">  family tour</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" href="#">  family tour</a>
                           </li> --}}
                        </ul>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </div>
   </div>