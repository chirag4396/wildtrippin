@extends('users.layouts.master')
@push('header')
<style type="text/css">
  
  .slider-data {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
  }

  .slider-data:hover {
    opacity: 1;
  }

  .slider-data::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
  }

  .slider-data::-moz-range-thumb {  
    width: 15px;
    height: 15px;
    background: #D19000;
    cursor: pointer;
  }
</style>
@endpush
@section('content')
	<section class="main-block" style="margin-top: 13%;">
  <div class="container">
    <div class="result-filter"  style="border:1px solid #eee; background:#fbf9f9;padding:10px;">
      <div class="row">
        <div class="col-12">
          <div class="row">
            <div class="col-12">
              <div class="data-text">
                @forelse($search_keywords as $pack_keys=>$pack_words)
                <button class="btn btn-default filter-button" data-filter="all"><i class=" fa fa-search"></i> {{ $pack_words }}</button>
                @empty
                @endforelse
              </div>
            </div>
           </div>
       </div>
     </div>

     
    
</div>
<div id="sorting-container" class="row">
  <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">
    <input type="hidden" name="searchtext" id="searchtext" value="{{ isset($narrowsearch)? $narrowsearch :'' }}">
    <input type="hidden" name="days" id="days" value="{{ isset($days)?$days:'' }}">
    <input type="hidden" name="cat_id" id="cat_id" value="{{ isset($cat_id)? $narrowsearch :'' }}">
    <input type="hidden" name="month" id="month" value="{{ isset($month)?$month:'' }}">
    <ul class="d-flex text-flex">
      <li><span class="span-disc">Narrow Search: {{ isset($narrowsearch)? str_limit($narrowsearch,14):'' }}<span></li><br>
        <li><span class="span-disc"><b>Name:{{ isset($name)?$name:'' }}</b></span></li><br>
        <li><span class="span-disc price"><b>Price:</b></span></li><br>
        <li><span class="span-disc"><b> No.of Days: {{ isset($days)?$days:'' }}</b></span></li><br>

      </ul>
    </div>
    <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">
      
      <!--     <input type="range" min="1" max="100" value="50" class="slider" id="myRange"> -->
      <div class="slidecontainer">
       <p>Price: <span id="demo"></span></p>
       <input type="range" min="{{ $min }}" max="{{ $max }}" value="50" class="slider-data" id="myRange">
       
     </div>

   <!--  <p for="amount">Price Range:
  <input type="range" min="1" max="100" value="50" class="" id="amount"></p>
<div id="slider-range"></div>
-->

</div>
<div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">
  <p class="psi_info text-right">
   {{ (count($packageData)>0) ? count($packageData) :  'No' }} Results found
  </p>
</div>
</div> 



<div class="row mt withoutajaxData">
    @forelse($packageData as $pk=>$pv)
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all">
    
        <div class="cuadro_intro_hover " style="background-color:#fff;">
          <p class="p1">
            @php 
                $image=\App\PackageImage::where('pi_pack_id',$pv->pack_id)->first();
            @endphp
            <img src="{{ asset($image['pi_img_path']) }}" class="img-responsive" alt="">
            
          </p>
          <div class="caption">
            <div class="blur"></div>
            <div class="caption-text">
              <a class=" btn btn-default button-book" href="{{ URL::to('/get-package-detail/'.$pv->pack_id) }}"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
              
              <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
                <span class="span-disc">Jumbo Discount Price From</span>&#8377;{{ $pv->pack_price }}</h3>
                <p>{{ $pv->pack_title }}</p>
                <p>{{ $pv->pack_days }} Days/{{ $pv->pack_nights }} nights:<span class="span-disc">ASHG</span></p>
                
              </div>
            </div>
          </div>
          <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
          {{ \App\City::find($pv->pack_city)->city_name }}</p> 

        </div>
    @empty
    @endforelse
    {{-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all">
      
      <div class="cuadro_intro_hover " style="background-color:#fff;">
        <p class="p1">
          <img src="images/a10.jpg" class="img-responsive" alt="">
        </p>
        <div class="caption">
          <div class="blur"></div>
          <div class="caption-text">
            <a class=" btn btn-default button-book" href="detail.html"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
            
            <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
              <span class="span-disc">Jumbo Discount Price From</span>&#8377;1,25,000</h3>
              <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
              <p>7 Days/6 nights:<span class="span-disc">ASHG</span></p>
              
            </div>
          </div>
          
        </div>
        <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
        Singapore</p>

      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all">
        
        <div class="cuadro_intro_hover " style="background-color:#fff;">
          <p class="p1">
            <img src="images/a9.jpg" class="img-responsive" alt="">
          </p>
          <div class="caption">
            <div class="blur"></div>
            <div class="caption-text">
              <a class=" btn btn-default button-book" href="detail.html"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
              
              <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
                <span class="span-disc">Jumbo Discount Price From</span>&#8377;1,25,000</h3>
                <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
                <p>7 Days/6 nights:<span class="span-disc">ASHG</span></p>
                
              </div>
            </div>
            
          </div>
          <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
          India</p>

        </div>

      </div> 


      <div class="row mt">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all">
         
          <div class="cuadro_intro_hover " style="background-color:#fff;">
            <p class="p1">
              <img src="images/a8.jpg" class="img-responsive" alt="">
            </p>
            <div class="caption">
              <div class="blur"></div>
              <div class="caption-text">
                <a class=" btn btn-default button-book" href="detail.html"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
                <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
                  <span class="span-disc">Jumbo Discount Price From</span>&#8377;1,25,000</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
                  <p>7 Days/6 nights:<span class="span-disc">ASHG</span></p>
                  
                </div>
              </div>
              
            </div>
            <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
            Singapore</p>

          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all">
            
            <div class="cuadro_intro_hover " style="background-color:#fff;">
              <p class="p1">
                <img src="images/a4.jpg" class="img-responsive" alt="">
              </p>
              <div class="caption">
                <div class="blur"></div>
                <div class="caption-text">
                 <a class=" btn btn-default button-book" href="detail.html"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
                 <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
                  <span class="span-disc">Jumbo Discount Price From</span>&#8377;1,25,000</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
                  <p>7 Days/6 nights:<span class="span-disc">ASHG</span></p>
                  
                </div>
              </div>
              
            </div>
            <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
            Africa  </p>

          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mt mtfilter all">
            
            <div class="cuadro_intro_hover " style="background-color:#fff;">
              <p class="p1">
                <img src="images/a6.jpg" class="img-responsive" alt="">
              </p>
              <div class="caption">
                <div class="blur"></div>
                <div class="caption-text">
                 <a class=" btn btn-default button-book" href="detail.html"><span class="glyphicon glyphicon-plus"> Book Now</span></a>
                 <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px; font-size: 17px;font-weight: 700;">
                  <span class="span-disc">Jumbo Discount Price From</span>&#8377;1,25,000</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
                  <p>7 Days/6 nights:<span class="span-disc">ASHG</span></p>
                  
                </div>
              </div>
              
            </div>
            <p class="locate-c"><i class="fa fa-map-marker" aria-hidden="true"></i>
            Singapore</p>

          </div> --}}

    </div>
    <div class="withajaxdata row mt" style="display: none;">
    </div>
<!-- 
</span> -->
</section>
@push('footer')
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  var slider = document.getElementById("myRange");
  var output = document.getElementById("demo");
  output.innerHTML = slider.value;

  slider.oninput = function() {
    output.innerHTML = this.value;
  }
</script>

<script>
  $(".map-icon").click(function() {
    $(".map-fix").toggle();
  });
</script>
<script>
        // Want to customize colors? go to snazzymaps.com
        function myMap() {
          var maplat = $('#map').data('lat');
          var maplon = $('#map').data('lon');
          var mapzoom = $('#map').data('zoom');
            // Styles a map in night mode.
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {
                lat: maplat,
                lng: maplon
              },
              zoom: mapzoom,
              scrollwheel: false
            });
            var marker = new google.maps.Marker({
              position: {
                lat: maplat,
                lng: maplon
              },
              map: map,
              title: 'We are here!'
            });
          }

          $('#myRange').change(function(){

            $('.price').html('Price:'+$(this).val()+' Rs');
            $.ajax({
                url: "{{ route('get-package-on-price') }}",
                type:'POST',
                data:{
                    price:$(this).val(),
                    month:$('#month').val(),
                    days:$('#days').val(),
                    searchtext:$('#searchtext').val(),
                    min:{{ $min }},
                    cat_id:$('#cat_id').val(),
                    "_token": "{{ csrf_token() }}"},
                    success: function(data){
                      console.log(data);
                      $('.withoutajaxData').hide();
                      $('.withajaxdata').show();
                        $('.withajaxdata').html(data);
                        
                    },
                    error: function(data){
                        console.log(data);
                    }

            });
          })
          
        </script>
        <!-- Map JS (Please change the API key below. Read documentation for more info) -->
        <script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyDMTUkJAmi1ahsx9uCGSgmcSmqDTBF9ygg"></script>
@endpush
@endsection