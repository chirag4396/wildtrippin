@extends('users.layouts.master')

@section('content')
<div id="mail" class="banner-bottom mail contact" style="padding-top: 5em;">
	<div class="container">
		<div class="title" style="padding-bottom: 0px;">
			<h2>Pay</h2>
		</div>
		<div class="w3ls_banner_bottom_grids">
			
			<div class="col-md-6 col-md-offset-3 w3layouts_mail_grid_left">
				<div class="agileits_mail_grid_left">
					<form method="post" class="comments-form" action="{{ route('placeorder') }}">
						{{ csrf_field() }}
						<input type="hidden" name="package" value="{{ $package->pack_id }}">
						<div class="form-group">
							<div class="no-padding">
								<label>Amount <span class="required">*</span></label>
								<input class="form-control pay-amount" name = "amount" type="number" min = "0" value="{{ (empty($posted['amount'])) ? $package->pack_cost : $posted['amount'] }}" readonly />
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<input hidden name="productinfo" value="{{  (empty($posted['productinfo'])) ? 'demo' : $posted['productinfo'] }}" size="64" />

						<input hidden name="surl" value="{{ (empty($posted['surl'])) ? route('orderStatus') : $posted['surl'] }}" size="64" />
						<input hidden name="furl" value="{{  (empty($posted['furl'])) ? route('orderStatus') : $posted['furl']  }}" size="64" />

						
						<div class="form-group">
							<label>Contact Person Name: <span class="required">*</span></label>	
							<input class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? '' : $posted['firstname'] }}" />

						</div>
						<div class="form-group">
							<label>Mobile Number: <span class="required">*</span></label>
							<input class="form-control" type="text" name="phone" value="{{ (empty($posted['phone'])) ? '' : $posted['phone'] }}" />

						</div>
						<div class="form-group">
							<label>Email ID: <span class="required">*</span></label>
							<input class="form-control" name="email" type="text" id="email" value="{{ (empty($posted['email'])) ? '' : $posted['email'] }}" />

						</div>				
						
						<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />
						
						<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />
						
						
						
						<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />
						
						<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />
						
						
						
						<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />
						<div class="form-group text-center">
							<input class = "btn btn-success" type="submit" value="Pay Now">
						</div>  
					</form>
					{{-- <form action="{{ $action }}" method="post" name="payuForm">
						{{ csrf_field() }}
						<input type="hidden" name="key" value="{{ $MERCHANT_KEY }}" />
						<input type="hidden" name="hash" value="{{ $hash }}"/>
						<input type="hidden" name="txnid" value="{{ $txnid }}" />
						<input type="hidden" name="service_provider" value="payu_paisa" size="64" />


						<input type = "hidden" name="productinfo" value="{{ (empty($posted['productinfo'])) ? 'MyFabShop' : $posted['productinfo'] }}" size="64" />

						<input type = "hidden" name="surl" value="{{ (empty($posted['surl'])) ? route('orderStatus') : $posted['surl'] }}" size="64" />

						<input type = "hidden" name="furl" value="{{  (empty($posted['furl'])) ? route('orderStatus') : $posted['furl']  }}" size="64" />

						<input type="hidden" name="package" value = "1">

						<div class="checkbox-form">		
							<div class="col-md-12">
								<div class="di-na bs">
									<label class="l-contact">
									</label>
									<input required class="form-control" readonly name="amount" type="number"  value="{{ (empty($posted['amount'])) ? $package->pack_cost : $posted['amount'] }}" />
								</div>
							</div>					
							<div class="col-md-12">
								<div class="di-na bs">
									<label class="l-contact">
										First Name 
										<em>*</em>
									</label>
									<input required class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? (Auth::user() ? Auth::user()->name : '') : $posted['firstname'] }}" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="di-na bs">
									<label class="l-contact">
										Email Address 
										<em>*</em>
									</label>
									<input required class="form-control" name="email" type="email" autocomplete="off" id="email" value="{{ (empty($posted['email'])) ? (Auth::user() ? Auth::user()->email : '') : $posted['email'] }}" />

								</div>
							</div>
							<div class="col-md-6">
								<div class="di-na bs">
									<label class="l-contact">
										Phone 
										<em>*</em>
									</label>
									<input required id = "phone" class="form-control" type="number" name="phone" value="{{ (empty($posted['phone'])) ? '' : $posted['phone'] }}" />

								</div>
							</div>										
							<div class="col-md-12">
								<label class="l-contact">
									Address  
									<em>*</em>
								</label>
								<div class="di-na bs">
									<textarea class="form-control" name="address" required>{{ (empty($posted['address']) ? '' : $posted['address']) }}</textarea>												
								</div>
							</div>											
							<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />

							<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />

							<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />

							<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />

							<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />

							<input type="hidden" name="pg" value="{{(empty($posted['pg'])) ? '' : $posted['pg'] }}" type="hidden" />
						</div>	
						<div class="clearfix"></div>

						<div class="text-center col-md-12" style="margin: 20px;">
							<input id = "proceedBtn" class="btn btn-success" type="submit" value="Submit">
						</div>

					</form> --}}
				</div>
			</div>
			
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@endsection