<?php

Route::get('/', function () {
    return view('users.index');
});

Route::get('/detail', function () {
    return view('users.detail');
});

Route::get('/list', function () {
    return view('users.listing');
});

Auth::routes();
Route::post('/enquiry', 'EnquiryController@store')->name('enquiry');
Route::post('/search-result', 'HomeController@searchResult')->name('search-result');
Route::get('/categorylist', 'CategoryController@getCategoryListing')->name('categorylist');
Route::get('/packagetitlelist', 'PackageController@getPackageTitleForSearch')->name('packagetitlelist');
Route::post('get-package-on-price', 'HomeController@getPackageOnPrice')->name('get-package-on-price');

Route::post('/reviews', 'ReviewController@store')->name('reviews');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getpackage-detail/{id}', 'HomeController@findPackageOnSubcategory')->name('package-details');
Route::get('/getpackage-listing/{id}','HomeController@findPackageOnCategory')->name('packages');
Route::get('/get-package-detail/{id}', 'HomeController@getPackageViewDetail')->name('package-view');
Route::post('/getstate/{id}','CityController@ajaxState');
Route::post('/getcity/{id}','CityController@ajaxcity');
Route::get('/getservice-package/{id}', 'HomeController@findPackageOnService')->name('services');

Route::post('/get-price-based-on-date', 'HomeController@findPriceOfEventDate')->name('price');


Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'PackageController@autocomplete'));

Route::get('/admin/login', function() {
	return view('admin.login');
});

//Route::resource('worker', 'UserDetailController');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function() {
	Route::get('/', function(){
		return view('admin.dashboard');
	})->name('home');

	// Route::get('images', function (){
		
	// 	return view('admin.upload');
	// })->name('g-images');
// 
	Route::get('all-categories', 'CategoryController@allCategories')->name('all-categories');

	Route::get('all-subcategories', 'SubCategoryController@allSubCategories')->name('all-subcategories');

	Route::get('all-indexategories', 'IndexCategoryController@allIndexCategories')->name('all-indexcategories');
	Route::get('all-packages', 'PackageController@allPackages')->name('all-packages');
	Route::get('all-vendors', 'UserController@allVendors')->name('all-vendors');
	Route::get('indexevents', 'PackageController@indexevents')->name('indexevents');
	Route::get('all-events', 'PackageController@allEvents')->name('all-events');
	Route::post('all-subcatofcat', 'PackageController@allSubcatOfCat')->name('all-subcatofcat');
	Route::get('all-services', 'ServiceController@allServices')->name('all-services');
	Route::get('all-enquiries', 'EnquiryController@allEnquiries')->name('all-enquiries');
	Route::get('all-reviews', 'ReviewController@allReviews')->name('all-reviews');

	Route::post('remove-day-itinerary', 'PackageController@removeItinarydays')->name('remove-day-itinerary');
	Route::post('remove-act-itinerary', 'PackageController@removeItinaryacts')->name('remove-act-itinerary');
	Route::post('remove-package-image', 'PackageController@removePackageImage')->name('remove-package-image');
	Route::post('view-enquiry', 'EnquiryController@ajaxViewEnquiry')->name('view-enquiry');
	Route::post('view-reviews', 'ReviewController@ajaxViewReview')->name('view-reviews');
	// Route::resource('qualification', 'QualificationController');
	// Route::resource('language', 'LanguageController');
	Route::resource('user', 'UserController');
	Route::resource('package', 'PackageController');
	Route::resource('category', 'CategoryController');
	Route::resource('sub-category', 'SubCategoryController');
	Route::resource('index-category','IndexCategoryController');
	Route::resource('service', 'ServiceController');
	Route::resource('enquiry', 'EnquiryController');
	Route::resource('review', 'ReviewController');
	// Route::resource('sub-category', 'SubCategoryController');
	// Route::resource('enquiry', 'EnquiryController');	
	// Route::resource('contact', 'ContactController');
	// Route::resource('payment', 'PaymentController');

});


//For Payment

Route::get('/pay','PaymentController@getPay')->name('get-pay');

Route::post('/pay-view','PaymentController@getPayView')->name('pay_view');

Route::post('/paydemo','PaymentController@getPay')->name('pay');

Route::post('/placeorder','PaymentController@getPay')->name('placeorder');

Route::post('/paystatus','PaymentController@orderStatus')->name('orderStatus');

Route::resource('payment', 'PaymentController');

Route::get('/booked/order/{id}/{status?}', 'PaymentController@previous')->name('previous');